#include "adconverter.h"

#if defined(__AVR__)
#include <avr/io.h>
#endif // __AVR__

#if defined(__AVR__)
void adConverterInit()
{
    //Disable Power Reduction Register ADC Bit
    PRR0 &= (0<<PRADC);
    //V Reference: AVCC with external capacitor on AREF pin
    ADMUX = _BV(REFS0);
    //Prescaler select bits
    //ADPS2 ADPS1 ADPS0 = 111 => 128 (division factor between XTAL and input clock)
    ADCSRA = _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);
    //Enable ADC
    ADCSRA |= _BV(ADEN);
     //Start conversion, Bit returns to zero after conversion
    ADCSRA |= _BV(ADSC);
    //Loop while conversion in progress
    while (ADCSRA & _BV(ADSC)) {}

    (void)ADC;
}

ADConverterValue adConverterRead(uint8_t pin)
{ 
  //Set input channel
  if(pin == 8) {
    //Set input channel selection bits MUX0 to MUX4 to 0
    ADMUX = (ADMUX & ~(0x1F));
    //Set input channel selection bit MUX5 to 1 (ADC8)
    ADCSRB |= _BV(MUX5);
  } else if(pin < 8) {
    //Set input channel bits to bitpattern of "pin" (ADC0-ADC7 = pin = 0-7)
    ADMUX = (ADMUX & ~(0x1F)) | (pin & 0x1F);
    //Set input channel selection bit MUX5 to 0
    ADCSRB |= ~(_BV(MUX5));
  //Temperature sensor selection
  } else if (pin == 39) {
    //Set input channel selection bits MUX0 to MUX4 to 00111
    ADMUX = (ADMUX & ~(0x1F)) | (pin & 0x1F);
    //Set input channel selection bit MUX5 to 1
    ADCSRB |= _BV(MUX5);
  }
  //Start conversion, Bit returns to zero after conversion
  ADCSRA |= _BV(ADSC);
  //Loop while conversion in progress
  while (ADCSRA & _BV(ADSC) ) {}

  ADConverterValue result = { ADC };
  return result;
}
#else
#include <stdlib.h>
void adConverterInit() { abort(); }
ADConverterValue adConverterRead(uint8_t pin) { abort(); (void)pin; }
#endif // __AVR__

/*!
    \union ADConverterValue
    \brief Represents an ADC value

    For easier conversation between values read from the
    ADC (16 bit) and the Remote Sensor APIs communication
    buffers (8 bit array) this union represents ADC values
    as both
    - an \ref ADConverterValue::value "uint16_t value"
    - an \ref ADConverterValue::valueArray "uint8_t array"
*/
/*!
    \var ADConverterValue::value
    \brief Represents ADC value as an uint16_t value
*/
/*!
    \var ADConverterValue::valueArray
    \brief Represents ADC value as an uint8_t array

    Use this field to copy the ADCs result into the
    Remote Sensor API buffer.
*/

/*!
    \struct ADConverter
    \brief This struct offers functions for using the
    ADC of an avr MCU

    \note Implementation for atmega328p
    \attention
    When compiling for "__AVR__" set the functions will
    be replaces with mocks just calling `abort()`.
*/
/*!
    \fn void ADConverter::init()
    \brief Initializes the ADC
*/
/*!
    \fn ADConverterValue ADConverter::read(uint16_t pin)
    \brief Reads ADC value

    The `pin` parameter defines the ADC which to
    read from.\n
    Returns an ADConverterValue object.
*/

struct ADConverter ADConverter = {
    .init = adConverterInit,
    .read = adConverterRead
};
