#pragma once

#include "terminaloutput.h"
#define UNITY_OUTPUT_START() TerminalOutput.init()
#define UNITY_OUTPUT_CHAR(a) TerminalOutput.putChar(a)
#define UNITY_OUTPUT_FLUSH() TerminalOutput.flush()
