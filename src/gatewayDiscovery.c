#include <gatewayDiscovery.h>
#include <remote-sensor/api.h>
#if defined(__AVR__)
#include <remote-sensor/communication/controller/watchdog.h>
#include <util/delay.h>
#endif // __AVR__

static const uint8_t *address;

#if defined(__AVR__)
/* Sets the address of the gateway */
void gatewayDiscoverySetAddress(const uint8_t *addressarg){
  address = addressarg;
}
#else
void gatewayDiscoverySetAddress(const unsigned char *addressarg){
  address = addressarg;
}
#endif // __AVR__

/* Send every 500ms a message for node discovery */
void gatewayDiscovery(void)
{
    //Every Node has to send a message on Startup in the Form: S0{location},B0W0U0R0C0O0O1A0 if sensor is on board  location as characters eg. BC013
    uint8_t sensorList[] = {'S','0','L','A','B',',','W','0','U','0','C','0','O','0','A','0'};
    while(connected == 0)
    {
        //Tap always the wdt because it has been initialized before
        CtrlRecovery.ok();

        //Send above message to the gateway
        CommunicationMiddlewareAPI.sendFrameTo64BitAddress(address, sensorList, sizeof(sensorList), 0);
        #if defined(__AVR__)
        _delay_ms(500);
        #endif // __AVR__
        //Handle answer from gateway
        if(CommunicationMiddlewareAPI.receivedNewFrame()){
            uint8_t pld[CommunicationMiddlewareAPI.getReceivedPayloadSize()];
            CommunicationMiddlewareAPI.getReceivedPayload(pld);
            //Just check if ACK (0x05) received in first byte of payload
            if(pld[0] == ACK){
               connected = 1;
            }
        }
    }

}
