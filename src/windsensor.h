#pragma once

#include "adconverter.h"
#include "apiobjects.h"

enum ADCchannel {
  ADC_Pin0 = 0x00,
  ADC_Pin1 = 0x01,
  ADC_Pin2 = 0x02,
  ADC_Pin3 = 0x03,
  ADC_Pin4 = 0x04,
  ADC_Pin5 = 0x05,
  ADC_Pin6 = 0x06,
  ADC_Pin7 = 0x07,
  ADC_Pin8 = 0x08,
};

bool readWindSensor(ApiObjects *apiObject, struct ADConverter *adConverter);
void readWindSensorHandle(StreamBuffers *, Request *, Parameter *[], Response *);
