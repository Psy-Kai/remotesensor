#pragma once

#if defined(__AVR__)
#include <avr/io.h>
#endif // __AVR__

static int connected = 0;

#if defined(__AVR__)
void gatewayDiscoverySetAddress(const uint8_t *addressarg);
#endif //AVR

void gatewayDiscovery(void);
