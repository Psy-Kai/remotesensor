#include "apiobjects.h"

/*!
    \brief Set member of \ref ApiObjects to `NULL`
    \attention `api_objects` need to be not `NULL`!
    No further checks are performed.
*/
void apiObjectsInitEmpty(ApiObjects *api_objects)
{
    api_objects->streamBuffers = NULL;
    api_objects->request = NULL;
    api_objects->response = NULL;
    api_objects->parameter = NULL;
    api_objects->bufferManager = NULL;
}
