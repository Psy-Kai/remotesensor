/*
 * adapter_BME680.h
 *
 *  Created on: 19.05.2018
 *      Author: Nicolas Frick
 */

/**
* @file	adapter_BME680.h
* @date	20 Mai 2018
* @version	1.0
* @brief
*/
/*! @file adapter_BME680.h
 *  @brief Adapter for BME680 sensor API
 *
 *  @note The adapter presets the sensor to communicate by TWI (Two Wire Interface aka I2C). The sensor will be
 * initialized as recommended in BOSCH API (e.g. filter oversampling settings). Refer to the manual and the
 * BME680_driver-master files: http://confluence.es.uni-due.de:8090/x/yICfAQ.
*/


#pragma once

/*! CPP guard */
#ifdef __cplusplus
extern "C"
{
#endif

/* Header includes */
#include <stdint.h>
#ifdef REMOTE_SENSOR_REGISTRATION
#include "apiobjects.h"
#endif

/*!
 *
 * @{*/
/******************************************************************************/
/*! @name		Common macros					      */
/******************************************************************************/
/** BME680 configuration macros */
/** Enable or un-comment the macro to provide floating point data output. Integer formats are used else. */
#ifndef BME680_FLOAT_POINT_COMPENSATION
#define BME680_FLOAT_POINT_COMPENSATION
#endif
/** The TWI sensor slave address. On Sensors- Daughterboard from IoT project 2018 only the secondary address is used */
#ifndef BME680_I2C_ADDR_PRIMARY
#define BME680_I2C_ADDR_PRIMARY   UINT8_C(0x76)
#endif
/** The TWI sensor slave address. On Sensors- Daughterboard from IoT project 2018 only the secondary address is used */
#ifndef BME680_I2C_ADDR_SECONDARY
#define BME680_I2C_ADDR_SECONDARY UINT8_C(0x77)
#endif

/** Use sensor with gas measurement */
#define BME680_GAS_SENSOR_ENABLED  UINT8_C(0x01)
/** Use sensor without gas measurement */
#define BME680_GAS_SENSOR_DISABLED UINT8_C(0x00)

/** BME680 Remote Sensor API first identifier*/
#define BME680_FIRST_HEADER_BYTE  (char) ('B')
/** BME680 Remote Sensor API second identifier */
#define BME680_SECOND_HEADER_BYTE (char) ('0')
/** @}*/

/* function prototype declarations */
/*!
 * @fn return_device_ptr_BME680
 * @brief Returns the pointer to the instance of the BME680 device structure.
 *
 * @see bme680_dev
 *
 * @return bme680_dev
 */
const struct bme680_dev *return_device_ptr_BME680(void);

/*!
 * @fn return_data_field_ptr_BME680
 * @brief Returns the pointer to the instance of the BME680 field_data structure.
 *
 * @see bme680_field_data
 *
 * @return bme680_field_data
 */
const struct bme680_field_data *return_data_field_ptr_BME680(void);

/*!
 *  @fn create_and_initalize_BME680_device
 *  @brief The function instanciates the device interfaces
 *  and configures as well as initializes the BME680 sensor with standard settings.
 *
 *  @param BME680_SLAVE_ADDRESS The TWI sensor slave address which is set by the PCB layout.
 *  @see BME680_I2C_ADDR_SECONDARY
 *
 *  @param BME680_GAS_SENSOR_USE Provide one of the BME680_GAS_SENSOR_ macros to enable or disable the gas measurement.
 *  @see BME680_GAS_SENSOR_USE
 *
 *  @return Result of API execution status
 *  @retval zero -> Success / +ve value -> Warning / -ve value -> Error
 */
int8_t create_and_initalize_BME680_device(uint8_t BME680_SLAVE_ADDRESS, uint8_t BME680_GAS_SENSOR_USE);

/*!
 * @fn read_sensor_data_BME680
 * @brief This internal API reads the BME680 measurements into the internal data_field structure which is initialized by the adapter API.
 *
 * @note This function calls the delay function internal. A delay will coccur during the reading of the sensor registers.
 * Before calling this function tap the watchdog before if used.
 * @see user_delay_ms
 *
 * @return Result of API execution status
 * @retval zero -> Success / +ve value -> Warning / -ve value -> Error
 */
int8_t read_sensor_data_BME680(void);

#ifdef REMOTE_SENSOR_REGISTRATION
/*!
 * @fn get_sensor_data_from_buffer_BME680
 * @brief This internal API reads the data from the internal data buffer into the provided stream buffer.
 * The function is called internal by the readBME680Handle function.
 *
 * @see readBME680Handle
 *
 * @see ApiObjects
 *
 * @param *apiObject	: The pointer to the Remote Sensor API objects. Only StreamBuffer is used.
 *
 * @return Result of API execution status
 */
bool get_sensor_data_from_buffer_BME680(ApiObjects *apiObject);

/*!
 * @fn readBME680Handle
 * @brief This API is the interface to the Remote Sensor API. The sensor will be read
 * and the readings will be passed to the buffer. Internal this function calls the read_sensor_data_BME680
 * and the get_sensor_data_from_buffer_BME680 function.
 *
 * @see PeripheralReadHandle
 *
 * @macro Define "REMOTE_SENSOR_REGISTRATION" in compiler flags to enable this function.
 *
 * @param StreamBuffers : The buffer to which the readings will be passed.
 * @param Request : The query from the gateway.
 * @param Parameter : Parameters from query.
 * @param Response : The response to the gateway.
 *
 */
void readBME680Handle(StreamBuffers *streamBuffers, Request *request,
                          Parameter *parameter[], Response *response);
#endif

#ifdef BME680_SELFTEST
/*!
 * @fn selftest_BME680
 * @brief Accesses the Self-test API for the BME680.
 *
 * @note This function takes long time to return, do not use with running wdt!
 *
 * @macro Define "BME680_SELFTEST" in compiler flags to enable this function.
 *
 * @return Error code
 * @retval 0	Success
 * @retval < 0	Error
 * @retval > 0	Warning
 *
 */
int8_t selftest_BME680(void);
#endif

#ifdef __cplusplus
}
#endif /* End of CPP guard */
