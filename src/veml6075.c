#ifdef __AVR__
#include <util/delay.h>
#endif //AVR
#include "veml6075.h"
#include "i2c.h"

/* VEML6075 slave address */
#define VEML6075_ADDR 0x10
/* Registers define */
#define VEML6075_CONF_REG 0x00
#define VEML6075_UVA_DATA_REG 0x07
#define VEML6075_UVB_DATA_REG 0x09
#define VEML6075_UVCOMP1_DATA_REG 0x0A
#define VEML6075_UVCOMP2_DATA_REG 0x0B
/* Register value define : CONF */
#define VEML6075_CONF_SD 0x01
#define VEML6075_CONF_UV_AF_AUTO 0x00
#define VEML6075_CONF_UV_AF_FORCE 0x02
#define VEML6075_CONF_UV_TRIG_NO 0x00
#define VEML6075_CONF_UV_TRIG_ONCE 0x04
#define VEML6075_CONF_HD 0x08
#define VEML6075_CONF_UV_IT_MASK 0x70
#define VEML6075_CONF_UV_IT_50MS 0x00
#define VEML6075_CONF_UV_IT_100MS 0x10
#define VEML6075_CONF_UV_IT_200MS 0x20
#define VEML6075_CONF_UV_IT_400MS 0x30
#define VEML6075_CONF_UV_IT_800MS 0x40
#define VEML6075_CONF_DEFAULT (VEML6075_CONF_UV_AF_AUTO | VEML6075_CONF_UV_TRIG_NO | VEML6075_CONF_UV_IT_100MS)

void readValues(unsigned char reg_addr, uint8_t *data_high, uint8_t *data_low){
  I2C.startWait((VEML6075_ADDR << 1) + I2C_WRITE);
  I2C.write(reg_addr);
  I2C.repStart((VEML6075_ADDR << 1) + I2C_READ);
  *data_low = I2C.readAck();
  *data_high = I2C.readAck();
  I2C.stop();
}

 void getUvValues(struct veml6075Data *data)
 {
    initUVSensor();
    readValues(VEML6075_UVA_DATA_REG, &(data->uva_high), &(data->uva_low));
    readValues(VEML6075_UVB_DATA_REG, &(data->uvb_high), &(data->uvb_low));
    readValues(VEML6075_UVCOMP1_DATA_REG, &(data->uvcomp1_high), &(data->uvcomp1_low));
    readValues(VEML6075_UVCOMP2_DATA_REG, &(data->uvcomp2_high), &(data->uvcomp2_low));
    enterSleepMode();
}

 void initUVSensor(void)
 {
     //shut down
     I2C.startWait((VEML6075_ADDR << 1) + I2C_WRITE);
     I2C.write(VEML6075_CONF_REG);
     I2C.write(VEML6075_CONF_DEFAULT | VEML6075_CONF_SD);
     I2C.write(0x00);
     I2C.stop();
     //reactivate
     I2C.startWait((VEML6075_ADDR << 1) + I2C_WRITE);
     I2C.write(VEML6075_CONF_REG);
     I2C.write(VEML6075_CONF_DEFAULT);
     I2C.write(0x00);
     I2C.stop();
     //Wait until sensor is ready
#ifdef __AVR__
     _delay_ms(200);
#endif//AVR

 }

 void enterSleepMode(void)
 {
     I2C.startWait((VEML6075_ADDR << 1) + I2C_WRITE);
     I2C.write(VEML6075_CONF_REG);
     I2C.write(VEML6075_CONF_DEFAULT | VEML6075_CONF_SD);
     I2C.write(0x00);
     I2C.stop();
 }


#ifndef LOCAL_TESTS
void readUvSensorHandle(StreamBuffers *streamBuffers, Request *request,
                          Parameter *parameter[], Response *response)
{ 
    struct veml6075Data data={0,0,0,0,0,0,0,0};
    struct BufferApi *bufferApi = &BufferManager;

    getUvValues(&data);

    setResponse(&data, streamBuffers, bufferApi, response);
}

#else
void readUvSensorHandle(StreamBuffers *streamBuffers, Request *request,
                           Parameter *parameter[], Response *response,
                          struct BufferApi *bufferManager, veml6075Data *data)
 {
     getUvValues(data);
     setResponse(data, streamBuffers, bufferManager, response);
 }
#endif

 void setResponse(struct veml6075Data *data, StreamBuffers *streamBuffers, struct BufferApi *bufferManager, Response *response)
 {
     bool success=true;
     if((data->uva_low == 0 && data->uva_high == 0) || (data->uvb_low == 0 && data->uvb_high == 0) ||
        (data->uvcomp1_low == 0 && data->uvcomp1_high == 0) || (data->uvcomp2_low == 0 && data->uvcomp2_high == 0))
         success=false;
     if(!success) {
       response->type = ERROR_FOUND;
     }
     else
     {
     if(!BufferManager.buffer_can_fit(10, streamBuffers->output))
     {
       createNewBuffer(streamBuffers, bufferManager);
     }
        bufferManager->push('U', streamBuffers->output);
        bufferManager->push('0', streamBuffers->output);
        bufferManager->push(data->uva_high, streamBuffers->output);
        bufferManager->push(data->uva_low, streamBuffers->output);
        bufferManager->push(data->uvb_high, streamBuffers->output);
        bufferManager->push(data->uvb_low, streamBuffers->output);
        bufferManager->push(data->uvcomp1_high, streamBuffers->output);
        bufferManager->push(data->uvcomp1_low, streamBuffers->output);
        bufferManager->push(data->uvcomp2_high, streamBuffers->output);
        bufferManager->push(data->uvcomp2_low, streamBuffers->output);

        response->word_size = sizeof(uint8_t);
        response->content_type = UNSIGNED_TYPE;
        response->data_length = 10;
        response->type = ACK;
     }
 }

void createNewBuffer(StreamBuffers *streamBuffers, struct BufferApi *bufferManager)
{
    bufferManager->destroy(streamBuffers->output);
    streamBuffers->output = bufferManager->create(10);
}
