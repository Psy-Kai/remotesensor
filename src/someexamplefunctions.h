#pragma once

#include <remote-sensor/api.h>

extern bool initResourceManager(struct ResourceManager *resourceManager);
extern bool initCommunicationController(struct CommunicationController *communicationController);
