#pragma once

/** defines the data direction (reading from I2C device) in i2c_start(),i2c_rep_start() */
#ifndef I2C_READ
#define I2C_READ    1
#endif
#ifndef I2C_WRITE
#define I2C_WRITE   0
#endif

struct I2C
{
    void (*init)(void);
    void (*stop)(void);
    unsigned char (*start)(unsigned char address);
    unsigned char (*repStart)(unsigned char address);
    void (*startWait)(unsigned char address);
    unsigned char (*write)(unsigned char data);
    unsigned char (*readAck)(void);
    unsigned char (*readNak)(void);
};

extern struct I2C I2C;
