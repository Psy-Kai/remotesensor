#include "tempsensor.h"

void createNewBufferForTempSensor(StreamBuffers *streamBuffers, struct BufferApi *bufferManager);

/* Reads the temperature sensor */
//This function fakes the BME680, only temperature is read via internal temperature sensor of the atmega32u4
//the remaining BME functionality is not available
bool readTempSensor(ApiObjects *apiObject, struct ADConverter *adConverter)
{
    if (!apiObject->bufferManager->buffer_can_fit(18, apiObject->streamBuffers->output))
        createNewBufferForTempSensor(apiObject->streamBuffers, apiObject->bufferManager);

    apiObject->bufferManager->push('B', apiObject->streamBuffers->output);
    apiObject->bufferManager->push('0', apiObject->streamBuffers->output);

    ADConverterValue value = adConverter->read(ADC_Temperature);
    if (value.valueArray[0] != apiObject->bufferManager->push(value.valueArray[0], apiObject->streamBuffers->output))
        return false;
    if (value.valueArray[1] != apiObject->bufferManager->push(value.valueArray[1], apiObject->streamBuffers->output))
        return false;
    //Fill the remaining bytes expected by the gateway for a bme680 response with 0
    for(uint8_t i = 0; i < 14; i++){
      apiObject->bufferManager->push(0, apiObject->streamBuffers->output);
    }

    apiObject->response->word_size = 0x12;
    apiObject->response->content_type = FLOAT_TYPE;
    return true;
}

/* The handle function to the RSApi */
void readTempSensorHandle(StreamBuffers *streamBuffers, Request *request,
                          Parameter *parameter[], Response *response)
{
    ApiObjects apiObjects = {
        .streamBuffers = streamBuffers,
        .request = request,
        .response = response,
        .parameter = parameter,
        .bufferManager = &BufferManager,
    };

    readTempSensor(&apiObjects, &ADConverter);
}

void createNewBufferForTempSensor(StreamBuffers *streamBuffers, struct BufferApi *bufferManager)
{
    bufferManager->destroy(streamBuffers->output);
    streamBuffers->output = bufferManager->create(18);
}
