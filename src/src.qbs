import qbs

Project {
    name: "Src"

    StaticLibrary {
        name: "Terminal Output"

        property int baudRate: 9600

        files: [
            "terminaloutput.c",
            "terminaloutput.h",
            "terminaloutputunitydefines.c",
            "terminaloutputunitydefines.h",
        ]

        cpp.defines: {
            var result = [];

            if (!qbs.architecture.startsWith("avr"))
                result.push("TERMINAL_OUTPUT_STDOUT");
            if (lufa.present)
                result.push("TERMINAL_OUTPUT_LIB_VIRTUAL_SERIAL")

            return result;
        }

        Depends {
            name: "cpp"
        }
        Depends {
            id: lufa
            condition: this.present
            name: "Lufa - Virtual Serial"
            required: false
        }
        Export {
            cpp.includePaths: product.sourceDirectory
            Depends {
                name: "cpp"
            }
        }
    }

    StaticLibrary {
        name: "Sensors"

        files: [
            "adconverter.c",
            "adconverter.h",
            "apiobjects.c",
            "apiobjects.h",
            "globaldefines.c",
            "i2c.c",
            "i2c.h",
            "someexamplefunctions.c",
            "someexamplefunctions.h",
            "windsensor.c",
            "windsensor.h",
            "apds_9250.c",
            "apds_9250.h",
            "veml6075.h",
            "veml6075.c",
            "co2sensor.h",
            "co2sensor.c",
            "omron_D7.c",
            "omron_D7.h",
            "gatewayDiscovery.c",
            "gatewayDiscovery.h",
            "tempsensor.c",
            "tempsensor.h",
            "changes_in_RSApi.txt",
            "XCTUrequests.txt",
        ]

        Group {
            name: "BME680"
            files: [
                "adapter_BME680.c",
                "adapter_BME680.h",
            ]
            condition: BME680Driver.present
            cpp.defines: ["BME680_SELFTEST", "REMOTE_SENSOR_REGISTRATION", "BME680_FLOAT_POINT_COMPENSATION"]
        }

        Depends {
            name: "Remote Sensor"
        }
        Depends {
            name: "I2C Master"
            required: false
        }
        Depends {
            name: "BME680Driver"
            required: false
        }
        Depends {
            name: "Terminal Output"
        }

        Depends {
            condition: this.present
            name: "Lufa - Virtual Serial"
        }

        Export {
            cpp.includePaths: product.sourceDirectory
            Depends {
                name: "cpp"
            }
            Depends {
                name: "Remote Sensor"
            }

            Depends {
                condition: this.present
                name: "Lufa - Virtual Serial"
            }

            Depends {
                name: "I2C Master"
                required: false
            }

            Depends {
                name: "BME680Driver"
                required: false
            }

        }
    }

    CppApplication {
        name: "Sensors Application"

        files: [
            "main.c",
        ]

        Depends {
            name: "avr"
        }

        Depends {
            name: "Sensors"
        }
    }

    AvrFlasher {
        property string app

        name: "AvrFlasher"
        condition: app

        Depends {
            name: product.app
        }
    }
}
