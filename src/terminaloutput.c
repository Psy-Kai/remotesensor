#include "terminaloutput.h"

#include <stdint.h>
#
#if defined(TERMINAL_OUTPUT_STDOUT)
#include <stdio.h>
#endif // TERMINAL_OUTPUT_STDOUT
#if defined(TERMINAL_OUTPUT_LIB_VIRTUAL_SERIAL)
#include <libVirtualSerial.h>
#endif // TERMINAL_OUTPUT_LIB_VIRTUAL_SERIAL

void resetTerminalOutput(struct TerminalOutput *terminal_output)
{
    terminal_output->buffer_pointer = terminal_output->buffer;
    for (uint8_t i = 0; i < TERMINAL_OUTPUT_BUFFER_SIZE; ++i)
        terminal_output->buffer_pointer[i] = 0;
}

bool terminalOutputInit()
{
#if defined(TERMINAL_OUTPUT_LIB_VIRTUAL_SERIAL)
    setup_virt_ser_port();
#endif // TERMINAL_OUTPUT_LIB_VIRTUAL_SERIAL

    resetTerminalOutput(&TerminalOutput);
    return true;
}

bool terminalOutputPutChar(char c)
{
    if(TerminalOutput.buffer_pointer < TerminalOutput.buffer)
        return false;
    if((TerminalOutput.buffer + TERMINAL_OUTPUT_BUFFER_SIZE) < TerminalOutput.buffer_pointer)
        return false;
    *TerminalOutput.buffer_pointer = c;
    ++TerminalOutput.buffer_pointer;

#if defined(TERMINAL_OUTPUT_STDOUT)
    putchar(c);
#endif // TERMINAL_OUTPUT_STDOUT

    return true;
}

void terminalOutputFlush()
{
#if defined(TERMINAL_OUTPUT_LIB_VIRTUAL_SERIAL)
    send_message(TerminalOutput.buffer);
#endif // TERMINAL_OUTPUT_LIB_VIRTUAL_SERIAL
    resetTerminalOutput(&TerminalOutput);
}

/*!
    \def TERMINAL_OUTPUT_BUFFER_SIZE
    \brief Defines size of the \ref TerminalOutput::buffer
    "internal buffer" of TerminalOutput
*/
/*!
    \struct TerminalOutput
    \brief Adapter for redirecting Unity output

    Implements the required
    [interface](https://github.com/ThrowTheSwitch/Unity/blob/master/docs/UnityConfigurationGuide.md#toolset-customization)
    to redirect the output made by the unity framework.\n
    The output will be redirected to the standart output
    or outputted via a virtual serial port (using LUFA).

    The TerminalOutput has an \ref TerminalOutput::buffer
    "internal buffer" which will be outputted when calling
    \ref TerminalOutput::flush "flush".
*/
/*!
    \fn bool TerminalOutput::init()
    \brief Initializes the TerminalOutput

    While TerminalOutput uses LUFA
    it needs to initialize the LUFA library.
    \note If no avr compuler is used this object
    will write to the standart output.
*/
/*!
    \fn bool TerminalOutput::putChar(char c)
    \brief Adds an character to the \ref TerminalOutput::buffer "internal buffer"

    Adds the parameter `c` to the internal buffer.

    Return true if the buffer could be appended, otherwise
    returns false.

    \note If the \ref TerminalOutput::buffer "buffer" is
    full the character wont be appended.
*/
/*!
    \fn void TerminalOutput::flush()
    \brief Flushes the \ref TerminalOutput::buffer "internal buffer"

    Outputs its content.
*/
/*!
    \var TerminalOutput::buffer
    \brief Internal Buffer

    Array of `char` with the size of
    \ref TERMINAL_OUTPUT_BUFFER_SIZE.
*/
/*!
    \var TerminalOutput::buffer_pointer
    \brief Pointer to the tail of the buffer

    This field points to the next free position in
    \ref TerminalOutput::buffer "buffer".
*/
struct TerminalOutput TerminalOutput = {
    .init = terminalOutputInit,
    .putChar = terminalOutputPutChar,
    .flush = terminalOutputFlush
};
