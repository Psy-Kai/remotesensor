#define REMOTE_SENSOR_REGISTRATION
#include <remote-sensor/api.h>
#include <i2c.h>
#include <adconverter.h>
#include <windsensor.h>
#include <apds_9250.h>
#include <veml6075.h>
#include <co2sensor.h>
//BME code doesn't fit on the flash with other sensors
//#include <adapter_BME680.h>
//Fake BME temperature sensor
#include <tempsensor.h>
#include <omron_D7.h>
#include <gatewayDiscovery.h>
#if defined (__AVR__)
#include <avr/io.h>
#endif //AVR

/* Function prototypes */
void addResource(char *uri, char *resourceName, PeripheralReadHandle handle);
void initSystem(void);

/* Gateway address, address size and maximum attempts for register function */
static const uint8_t address[] = {0x00, 0x13, 0xA2, 0x00, 0x41, 0x89, 0x4D, 0xDF};
static const size_t address_size = 8;
static const uint8_t NUMBER_OF_ATTEMPTS = 0x80;

/* Adds a resource to the RSApi, standart procedure as shown in RSApi app example */
void addResource(char *uri, char *resourceName, PeripheralReadHandle handle)
{
    Resource *resource = ResourceManager.create(resourceName, IS_RESETTABLE, SINGLE_BUFFER,
                                                           RESOURCE_MANAGER_DEFAULT);
    PeripheralReadHandle read_handle = handle;
    ResourceManager.attach_read_handle(read_handle, resource);
    PeripheralCallHandle call_handle = handle;
    ResourceManager.attach_call_handle(call_handle, resource);

    URIComponent *uriComponent = ResourceManager.create_identifier(uri);
    ResourceManager.register_resource(resource, uriComponent);
    ResourceManager.register_component(uriComponent, ResourceManager.get_root_component());
}

/* Initializes the ADC (windsensor), I2C module, GatewayDiscovery, CommunicationController,
   some sensors and add sensors to the RSApi */
void initSystem(void)
{
#if defined(__AVR__)
    ADConverter.init();
    I2C.init();
    //Set PORT6+7 on DDRC to output to signal state registered and sent frame
    DDRC |= _BV(DDC6) | _BV(DDC7);
#endif // __AVR__
    initUVSensor();
    //Omron init takes several seconds depending on its state, if stuck (earthquake occured) remove power supply
    initOmron();
    initAPDS9250Sensor();

    //BME code doesn't fit on the flash with other sensors
    //create_and_initalize_BME680_device(BME680_I2C_ADDR_SECONDARY, BME680_GAS_SENSOR_ENABLED);

    //Set gateway address in the discovery function
    gatewayDiscoverySetAddress(address);

    //Init CommunicationController
    uint8_t buffer_size = 0x06;
    uint8_t params_size = 0x05;
    size_t stream_request_list_size = 5;
    RequestAcceptType accept_requests = ACCEPT_STREAM;
    CommunicationController.init(accept_requests, stream_request_list_size, params_size, buffer_size);

    //Register sensors, identifier for the sensors required by gateway: A0 = APDS, B0 = BME680...
    addResource("W0", "read", &readWindSensorHandle);
    addResource("A0", "read", &readAPDS9250SensorHandle);
    addResource("U0", "read", &readUvSensorHandle);
    addResource("C0", "read", &readCo2SensorHandle);
    addResource("O0", "read", &readOmronD7sHandle);
    //BME code doesn't fit on the flash with other sensors
    //addResource("B0", "read", &readBME680Handle);
    //Fake BME temperature sensor, remaining data be set to 0
    addResource("B0", "read", &readTempSensorHandle);

}


int main(int argc, char *argv[])
{

    (void)argc;
    (void)argv;

    //Do the initialisations
    initSystem();

    //Send message with localisation of node and active sensors every half second,
    //wait for simple ACK from gateway (see XCTUrequests.txt)
    gatewayDiscovery();

    //Standart procedure from confluence documentation of RSApi
    RegistrationController.register_to(address, address_size, NUMBER_OF_ATTEMPTS);

    #if defined(__AVR__)
    //Set LED PORTC6 high after succesfull register process
    PORTC |= _BV(PORTC6);
    #endif // __AVR__

    //Standart procedure from confluence documentation of RSApi
    while (true) {
        CommunicationController.listen(address, address_size);
    }
}

