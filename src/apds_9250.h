#pragma once

#include <inttypes.h>
#include <remote-sensor/api.h>
#include "i2c.h"

/*
 * apds_9250.h
 *
 *      Author: Michael Hildebrand
 */

/*! \file apds_9250.h
 * \brief APDS 9250 RGB Sensor Documentation.
 *
 */
typedef struct apds9250Data {
    /*!
     * \brief Struct that holds the Sensor data.
     * \struct apds9250Data
     * Holds the High, Medium and Low Byte from R, G, B & W Register from apds9250 Sensor.
     */
    uint8_t r_high;
    uint8_t r_med;
    uint8_t r_low;
    uint8_t g_high;
    uint8_t g_med;
    uint8_t g_low;
    uint8_t b_high;
    uint8_t b_med;
    uint8_t b_low;
    uint8_t w_high;
    uint8_t w_med;
    uint8_t w_low;
} apds9250Data;


#if defined(__AVR__)
void readAPDS9250SensorHandle(StreamBuffers *, Request *, Parameter *[], Response *);
#else
void readAPDS9250SensorHandle(StreamBuffers *, Request *, Parameter *[], Response *,
                              struct BufferApi *, struct apds9250Data *);
#endif

/*! \fn void readAPDS9250SensorHandle(StreamBuffers *streamBuffers, Request *request,
                          Parameter *parameter[], Response *response)
 * \brief APDS9250 RGB Sensor Handle function.
 *
 * This function gets attached to the Remote Sensor API (see PeripheralReadHandle & ResourceManager.attach_read_handle from RS API).
 * This funtcion get called from Remote Sensor API. It calls getAPDS9250Values function and writes the return
 * into the bufferManager and setups the response with setResponse function.
 * \param streamBuffers Output Stream for Payload.
 * \param request Holds the request.
 * \param parameter Parameters from the query.
 * \param response The Response.
*/
#if defined(__AVR__)
void readAPDS9250SensorHandle(StreamBuffers *, Request *, Parameter *[], Response *);
#else
void readAPDS9250SensorHandle(StreamBuffers *, Request *, Parameter *[], Response *, struct BufferApi *, struct apds9250Data *);
#endif



void getAPDS9250Values(struct apds9250Data *);
/*! \fn void getAPDS9250Values(struct apds9250Data *data)
 * \brief Reads Data from Sensor.
 *
 * The data from the Sensor get saved in struct apds9250Data
 */
#if defined(__AVR__)
void getAPDS9250Values(struct apds9250Data *);
#else
void getAPDS9250Values(struct apds9250Data *);
#endif



/*! \fn void setAPDS9250Response(struct apds9250Data *, StreamBuffers *, struct BufferApi *, Response *);
 * \brief setAPDS9250Response - Sets the response.
 *
 * Sets the Response and handles the BufferManager.
 * Response.type is set to ERROR_FOUND if the struct apds9250Data contains a R,G,B or W that is '0'.
 */
void setAPDS9250Response(struct apds9250Data *, StreamBuffers *, struct BufferApi *, Response *);



/*!
 * \brief createNewBuffer Creates a new Buffer and destroys the old Buffer.
 */
void createNewBufferForAPDS9250(StreamBuffers *, struct BufferApi *);



/*!
 * \brief initAPDS9250Sensor Sets initial settings for apds9250 Sensor.
 *
 * APDS9250 Configuration / Settings
 * Control Code: 0x06,
 * MEAS_RATE = 0x04;
 * GAIN = 0x01;
 * Result in All RGB+IR Components active
 * Resolution / bit width = 20 bit (400ms)
 * Measurement Rate = 500ms
 */
 #if defined(__AVR__)
void initAPDS9250Sensor(void);
#else
void initAPDS9250Sensor(void);
#endif
