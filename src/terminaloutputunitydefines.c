#include "terminaloutputunitydefines.h"

/*!
    \file terminaloutputunitydefines.h

    Include this file before including `unity.h`.

    \note No need to include this file while unity
    will be configured to use the macros automaticly
    in this project.
*/
/*!
    \def UNITY_OUTPUT_START
    \brief Calls \ref TerminalOutput::init

    Unity uses this macro to initializes the output.
*/
/*!
    \def UNITY_OUTPUT_CHAR
    \brief Calls \ref TerminalOutput::putChar

    Unity uses this macro to output one character.\n
    In this implementation the character will be
    put into an buffer.
*/
/*!
    \def UNITY_OUTPUT_FLUSH
    \brief Calls \ref TerminalOutput::flush

    Unity uses this macro to push a potential buffer.\n
    In this implementation the internal buffer of
    \ref TerminalOutput will be flushed.
*/
