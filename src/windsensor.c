#include "windsensor.h"

bool readWindSensor(ApiObjects *apiObject, struct ADConverter *adConverter)
{
    if (!apiObject->bufferManager->buffer_can_fit(4, apiObject->streamBuffers->output))
        return false;

    apiObject->bufferManager->push('W', apiObject->streamBuffers->output);
    apiObject->bufferManager->push('0', apiObject->streamBuffers->output);

    ADConverterValue value = adConverter->read(ADC_Pin8);
    if (value.valueArray[0] != apiObject->bufferManager->push(value.valueArray[0], apiObject->streamBuffers->output))
        return false;
    if (value.valueArray[1] != apiObject->bufferManager->push(value.valueArray[1], apiObject->streamBuffers->output))
        return false;

    apiObject->response->word_size = 0x02;
    apiObject->response->content_type = UNSIGNED_TYPE;
    return true;
}

void readWindSensorHandle(StreamBuffers *streamBuffers, Request *request,
                          Parameter *parameter[], Response *response)
{
    ApiObjects apiObjects = {
        .streamBuffers = streamBuffers,
        .request = request,
        .response = response,
        .parameter = parameter,
        .bufferManager = &BufferManager,
    };

    readWindSensor(&apiObjects, &ADConverter);
}
