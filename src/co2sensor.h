#pragma once

#include <stdint.h>
#include <remote-sensor/api.h>

bool readCo2Sensor(uint16_t *co2, uint16_t *tvoc);
void readCo2SensorHandle(StreamBuffers *, Request *, Parameter *[], Response *);
