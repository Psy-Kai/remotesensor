#pragma once

/* This code fakes the BME680, only temperature is read via internal temperature sensor of the atmega32u4
   the remaining BME functionality is not available */

#include "adconverter.h"
#include "apiobjects.h"

static uint8_t ADC_Temperature = 0x27;

bool readTempSensor(ApiObjects *apiObject, struct ADConverter *adConverter);
void readTempSensorHandle(StreamBuffers *, Request *, Parameter *[], Response *);
