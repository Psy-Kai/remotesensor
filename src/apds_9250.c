#include "apds_9250.h"

/*
 * apds_9250.c
 *
 *      Author: Michael Hildebrand
 */

//-----------------------------------------------------------------------------
// Definition for APDS 9250 registers
//-----------------------------------------------------------------------------
const unsigned char APDS9250_SLAVE_ADD = 0x52;
const unsigned char APDS_MAIN_CTRL_ADD = 0x00;
const unsigned char APDS_MAIN_CTRL = 0x06;
const unsigned char APDS_MEAS_RATE_ADD = 0x04;
const unsigned char APDS_MEAS_RATE = 0x04;
const unsigned char APDS_GAIN_ADD = 0x05;
const unsigned char APDS_GAIN = 0x01;
const unsigned char APDS_READ_ADD = 0x0A;

//----------------------------------------------------------------------------
// APDS9250 Sensor Handle function
//----------------------------------------------------------------------------
#if defined(__AVR__)
void readAPDS9250SensorHandle(StreamBuffers *streamBuffers, Request *request,
                          Parameter *parameter[], Response *response)
{ 
    struct apds9250Data data={0,0,0,0,0,0,0,0,0,0,0,0};  /*!< Struct that gets Data from function getAPDS9250Values. */
    struct BufferApi *bufferApi = &BufferManager;        /*!< Struct that holds the bufferManager */

    getAPDS9250Values(&data);

    setAPDS9250Response(&data, streamBuffers, bufferApi, response);
}
#else
 void readAPDS9250SensorHandle(StreamBuffers *streamBuffers, Request *request,
                           Parameter *parameter[], Response *response,
                          struct BufferApi *bufferManager, apds9250Data *data)
 {
     getAPDS9250Values(&data);
     setAPDS9250Response(&data, streamBuffers, bufferManager, response);
 }
#endif

//----------------------------------------------------------------------------
// Reads Data from Sensor.
//----------------------------------------------------------------------------
#if defined(__AVR__)
 void getAPDS9250Values(struct apds9250Data *data)
 {
    uint8_t buff[12];
    //start I2C communication
    I2C.startWait((APDS9250_SLAVE_ADD << 1) + I2C_WRITE);
    I2C.write(APDS_READ_ADD);
    I2C.repStart((APDS9250_SLAVE_ADD << 1) + I2C_READ);
    //load all values into buffer
    for(uint8_t i=0; i<12; i++){
        if(i==11) buff[i] = I2C.readNak();
        else buff[i] = I2C.readAck();
    }
    I2C.stop();

    data->w_low = buff[0];
    data->w_med = buff[1];
    data->w_high = buff[2];
    data->g_low = buff[3];
    data->g_med = buff[4];
    data->g_high = buff[5];
    data->b_low = buff[6];
    data->b_med = buff[7];
    data->b_high = buff[8];
    data->r_low = buff[9];
    data->r_med = buff[10];
    data->r_high = buff[11];
}
#else
  void getAPDS9250Values(struct apds9250Data *data) //only for Local Tests
 {
    uint8_t buff[12];
    //start I2C communication
    I2C.startWait((APDS9250_SLAVE_ADD << 1) + I2C_WRITE);
    I2C.write(APDS_READ_ADD);
    I2C.repStart((APDS9250_SLAVE_ADD << 1) + I2C_READ);
    //load all values into buffer
    for(uint8_t i=0; i<12; i++){
        if(i==11) buff[i] = I2C.readNak();
        else buff[i] = I2C.readAck();
    }
    I2C.stop();

    data->w_low = buff[0];
    data->w_med = buff[1];
    data->w_high = buff[2];
    data->g_low = buff[3];
    data->g_med = buff[4];
    data->g_high = buff[5];
    data->b_low = buff[6];
    data->b_med = buff[7];
    data->b_high = buff[8];
    data->r_low = buff[9];
    data->r_med = buff[10];
    data->r_high = buff[11];
}
#endif
//----------------------------------------------------------------------------
// helper functions
//----------------------------------------------------------------------------
 void setAPDS9250Response(struct apds9250Data *data, StreamBuffers *streamBuffers, struct BufferApi *bufferManager, Response *response)
 {
     bool success=true;
     if((data->r_low == 0 && data->r_med == 0) || (data->b_low == 0 && data->b_med == 0) ||
        (data->g_low == 0 && data->g_med == 0) || (data->w_low == 0 && data->w_med == 0))
         success=false;
     if(!success) {
       response->type = ERROR_FOUND;
     }
     else
     {
        if(!BufferManager.buffer_can_fit(14, streamBuffers->output))
        {
           createNewBufferForAPDS9250(streamBuffers, bufferManager);
        }
        bufferManager->push('A', streamBuffers->output); //Uri
        bufferManager->push('0', streamBuffers->output);//Uri
        bufferManager->push(data->r_high, streamBuffers->output);
        bufferManager->push(data->r_med, streamBuffers->output);
        bufferManager->push(data->r_low, streamBuffers->output);
        bufferManager->push(data->g_high, streamBuffers->output);
        bufferManager->push(data->g_med, streamBuffers->output);
        bufferManager->push(data->g_low, streamBuffers->output);
        bufferManager->push(data->b_high, streamBuffers->output);
        bufferManager->push(data->b_med, streamBuffers->output);
        bufferManager->push(data->b_low, streamBuffers->output);
        bufferManager->push(data->w_high, streamBuffers->output);
        bufferManager->push(data->w_med, streamBuffers->output);
        bufferManager->push(data->w_low, streamBuffers->output);

        response->word_size = sizeof(uint8_t);
        response->content_type = UNSIGNED_TYPE;
        response->data_length = 14;
        response->type = ACK;
     }
 }

void createNewBufferForAPDS9250(StreamBuffers *streamBuffers, struct BufferApi *bufferManager)
{
    bufferManager->destroy(streamBuffers->output);
    streamBuffers->output = bufferManager->create(14);
}

#if defined(__AVR__)
void initAPDS9250Sensor(void)
{
    I2C.startWait((APDS9250_SLAVE_ADD << 1) + I2C_WRITE);
    I2C.write(APDS_MAIN_CTRL_ADD);
    I2C.write(APDS_MAIN_CTRL);
    I2C.write(APDS_MEAS_RATE);
    I2C.write(APDS_GAIN);
    I2C.stop();
}
#else
void initAPDS9250Sensor(void) //only for Local Tests
{
  I2C.startWait((APDS9250_SLAVE_ADD << 1) + I2C_WRITE);
  I2C.write(APDS_MAIN_CTRL_ADD);
  I2C.write(APDS_MAIN_CTRL);
  I2C.write(APDS_MEAS_RATE);
  I2C.write(APDS_GAIN);
  I2C.stop();
}
#endif
