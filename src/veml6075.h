#include <inttypes.h>
#include <remote-sensor/api.h>

typedef struct veml6075Data {

    uint8_t uva_high;
    uint8_t uva_low;
    uint8_t uvb_high;
    uint8_t uvb_low;
    uint8_t uvcomp1_high;
    uint8_t uvcomp1_low;
    uint8_t uvcomp2_high;
    uint8_t uvcomp2_low;
} veml6075Data;

#ifndef LOCAL_TESTS
    void readUvSensorHandle(StreamBuffers *, Request *, Parameter *[], Response *);
#else
    void readUvSensorHandle(StreamBuffers *, Request *, Parameter *[], Response *, struct BufferApi *, struct veml6075Data *);
#endif
    void readValues(unsigned char reg_addr, uint8_t *data_high, uint8_t *data_low);
    void getUvValues(struct veml6075Data *data);
    void setResponse(struct veml6075Data *, StreamBuffers *, struct BufferApi *, Response *);
    void createNewBuffer(StreamBuffers *streamBuffers, struct BufferApi *bufferManager);
    void initUVSensor(void);
    void enterSleepMode(void);
