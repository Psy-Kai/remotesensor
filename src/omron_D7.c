#if defined(__AVR__)
#include <avr/io.h>
#include <util/delay.h>
#endif// __AVR__
#include "omron_D7.h"
#include "i2c.h"

/* Omron slave address and addresses of first data byte Main_SI_high/low */
static const unsigned char OmronSlave = 0x55;
static const unsigned char highAddressFirstDataByte = 0x20;
static const unsigned char lowAddressFirstDataByte = 0x00;

/*The data storage */
static struct omrond7sData data;

/* Write to a single register */
uint8_t writeSingleRegisterOmron(unsigned char addressHigh, unsigned char addressLow, unsigned char data){
  unsigned char res = 0;

  I2C.startWait((OmronSlave << 1) + I2C_WRITE);
  res = I2C.write(addressHigh);
  if(res != 0) return res;
  res = I2C.write(addressLow);
  if(res != 0) return res;
  res = I2C.write(data);
  if(res != 0) return res;
  I2C.stop();

  return res;
}

/* Read from a single register */
uint8_t readSingleRegisterOmron(unsigned char addressHigh, unsigned char addressLow){
  unsigned char read = 0;

  I2C.startWait((OmronSlave << 1) + I2C_WRITE);
  I2C.write(addressHigh);
  I2C.write(addressLow);
  I2C.repStart((OmronSlave << 1) + I2C_READ);
  read = I2C.readNak();
  I2C.stop();

  return read;
}

/* Read from multiple register */
uint8_t readMultipleRegistersOmron(unsigned char addressHigh, unsigned char addressLow, uint8_t *buff, size_t size){
  unsigned char res = 0;
  size_t lastByte = 0;
  if(size > 0){
    lastByte = size -1;
  }

  I2C.startWait((OmronSlave << 1) + I2C_WRITE);
  res = I2C.write(addressHigh);
  res = I2C.write(addressLow);
  res = I2C.repStart((OmronSlave << 1) + I2C_READ);
  for(uint8_t i=0; i < size; i++)
  {
      if(i == (lastByte)){
          buff[i] = I2C.readNak();
        }else{
          buff[i] = I2C.readAck();
        }
  }
  I2C.stop();

  return res;
}

/* Inits the sensor to inital sensor settings */
//Depending on the state the init process takes several time
void initOmron(void){
  //Wait for stdby
  while(getCurrentStatusOmron() != NORMAL_MODE_STDBY){}
  //Set the SETTING pin high
#if defined(__AVR__)
  DDRF |= _BV(DDF7);
  PORTF |= _BV(PORTF7);
#endif// __AVR__
  //Set switch axes at installation
  setCurrentCTRLOmron(CTRL_AXIS_INST_SWITCH);
#if defined (__AVR__)
  _delay_ms(2000);
#endif // __AVR__
  //Set initial installation mode
  setCurrentModeOmron(INITIAL_INST_MODE);
  //Wit for stdby
  while(getCurrentStatusOmron() != NORMAL_MODE_STDBY){}
  //Clear EVENT register
  getCurrentEventsOmron();
}

/* Signal occuring earthquake, state 0x01 in status register */
uint8_t earthquakeFlag(void){
  return (getCurrentStatusOmron() == NORMAL_MODE);
}

/* Get status register */
uint8_t getCurrentStatusOmron(void){
  return (readSingleRegisterOmron(highAddressCMD_STATUS_REGISTERS, lowAddressSTATE) & 0x07);
}

/* Get axis register */
uint8_t getCurrentAxisStateOmron(void){
  return readSingleRegisterOmron(highAddressCMD_STATUS_REGISTERS, lowAddressAXIS_STATE);
}

/* Get events register, the register is cleared automatical after reading */
uint8_t getCurrentEventsOmron(void){
  return readSingleRegisterOmron(highAddressCMD_STATUS_REGISTERS, lowAddressEVENT);
}

/* Get mode register */
uint8_t getCurrentModeOmron(void){
  return readSingleRegisterOmron(highAddressCMD_STATUS_REGISTERS, lowAddressMODE);
}

/* Set mode register */
uint8_t setCurrentModeOmron(uint8_t mode){
  return writeSingleRegisterOmron(highAddressCMD_STATUS_REGISTERS, lowAddressMODE, mode);
}

/* Get ctrl register */
uint8_t getCurrentCTRLOmron(void){
  return readSingleRegisterOmron(highAddressCMD_STATUS_REGISTERS, lowAddressCTRL);
}

/* Set ctrl register. Read out register, save the first nibble and write ctrl */
uint8_t setCurrentCTRLOmron(uint8_t ctrl){
  uint8_t reg = getCurrentCTRLOmron();
  reg = (uint8_t) ((ctrl << 4) | (reg & 0x0F));
  return writeSingleRegisterOmron(highAddressCMD_STATUS_REGISTERS, lowAddressCTRL, reg);
}

/* Clear all data registers */
uint8_t clearOmronRegisters(void){
  return writeSingleRegisterOmron(highAddressCMD_STATUS_REGISTERS, lowAddressCLEAR_COMMAND, CLEAR_COMMAND);
}

/* Get latest earthquake data */
struct omrond7sData *getLatestOmronValues (void)
{
        //Create data buffer
        uint8_t buff[4];
        //Set buffer to 0
        memset(buff,0,4);
        //Read n data registers, starting with first data register, n = size
        readMultipleRegistersOmron(highAddressFirstDataByte, lowAddressFirstDataByte, buff, sizeof (buff));
        //Clear the events
        getCurrentEventsOmron();
        //Clear all data
        clearOmronRegisters();
        //Put data in storage
	/*Earthquake-Related Data   0x 20 00 */
	data.MAIN_SI_H = buff[0];
	data.MAIN_SI_L = buff[1];
	data.MAIN_PGA_H = buff[2];
	data.MAIN_PGA_L = buff[3];

        return &data;
}

/* Set response for RSApi */
void setLatestOmronResponse(StreamBuffers *streamBuffers, struct BufferApi *bufferManager, Response *response)
{

	  if(!BufferManager.buffer_can_fit(6, streamBuffers->output))
	    createNewBufferForOmronD7s(streamBuffers, bufferManager);
	  bufferManager->push('O', streamBuffers->output);
	  bufferManager->push('0', streamBuffers->output);
	  bufferManager->push(data.MAIN_SI_H, streamBuffers->output);
	  bufferManager->push(data.MAIN_SI_L, streamBuffers->output);
	  bufferManager->push(data.MAIN_PGA_H, streamBuffers->output);
	  bufferManager->push(data.MAIN_PGA_L, streamBuffers->output);

          response->word_size = sizeof(uint8_t);
          response->content_type = UNSIGNED_TYPE;
          response->data_length=6;
          response->type = ACK;
}

#ifndef LOCAL_TESTS
/* The interface function to RSApi */
void readOmronD7sHandle(StreamBuffers * streamBuffers, Request *request, Parameter *parameter[], Response *response)
{
        memset(&data,0,4);
        struct BufferApi *bufferApi = &BufferManager;

	getLatestOmronValues();

	setLatestOmronResponse(streamBuffers, bufferApi, response);


}
#else
/* Test interface */
void readOmronD7sHandle(StreamBuffers * streamBuffers, Request *request,
Parameter *parameter[], Response *response, struct BufferApi *bufferManager)
{
	getLatestOmronValues();

}
#endif

/* Creates new buffer */
void  createNewBufferForOmronD7s(StreamBuffers *streamBuffers, struct BufferApi *bufferManager)
{
	bufferManager->destroy(streamBuffers->output);
	streamBuffers->output = bufferManager->create(62);
}

/* Merges two bytes to a 16bit uint */
uint16_t mergeTwoBytes(uint8_t highByte, uint8_t lowByte){
  return (uint16_t) ((highByte << 8) | lowByte);
}

/* Cast 16bit uint data to float */
float castToFloat(uint8_t highByte, uint8_t lowByte){
   return ((float) ((uint16_t) mergeTwoBytes(highByte, lowByte))) / 1000;
}

/* Cast 16bit uint temperature data to float */
float castTempToFloat(uint8_t highByte, uint8_t lowByte){
   return ((float) ((int16_t) mergeTwoBytes(highByte, lowByte))) / 10;
}
