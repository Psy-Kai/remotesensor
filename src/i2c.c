#include "i2c.h"

#include <stdlib.h>

#ifdef I2CMASTER
#include <i2cmaster.h>
#endif // I2CMASTER

#ifdef I2CMASTER
struct I2C I2C = {
    .init = i2c_init,
    .stop = i2c_stop,
    .start = i2c_start,
    .repStart = i2c_rep_start,
    .startWait = i2c_start_wait,
    .write = i2c_write,
    .readAck = i2c_readAck,
    .readNak = i2c_readNak
};
#else
void i2cInitMock() { abort(); }
void i2cStopMock() { abort(); }
unsigned char i2cStartMock(unsigned char address) { abort(); }
unsigned char i2cRepStartMock(unsigned char address) { abort(); }
void i2cStartWaitMock(unsigned char address) { abort(); }
unsigned char i2cWriteMock(unsigned char data) { abort(); }
unsigned char i2cReadAckMock() { abort(); }
unsigned char i2cReadNakMock() { abort(); }
unsigned char i2cReadMock(unsigned char ack) { abort(); }

struct I2C I2C = {
    .init = i2cInitMock,
    .stop = i2cStopMock,
    .start = i2cStartMock,
    .repStart = i2cRepStartMock,
    .startWait = i2cStartWaitMock,
    .write = i2cWriteMock,
    .readAck = i2cReadAckMock,
    .readNak = i2cReadNakMock
};
#endif // I2CMASTER
