#pragma once

#include <stdbool.h>

#if !defined(TERMINAL_OUTPUT_BUFFER_SIZE)
#define TERMINAL_OUTPUT_BUFFER_SIZE 255
#endif // TERMINAL_OUTPUT_BUFFER_SIZE

struct TerminalOutput
{
    bool (*init)();
    bool (*putChar)(char c);
    void (*flush)();
    char buffer[TERMINAL_OUTPUT_BUFFER_SIZE];
    char *buffer_pointer;
};

extern struct TerminalOutput TerminalOutput;
