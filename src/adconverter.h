#pragma once

#include <stdint.h>

typedef union
{
    uint16_t value;
    uint8_t valueArray[sizeof(uint16_t)];
} ADConverterValue;

struct ADConverter
{
    void (*init)(void);
    ADConverterValue (*read)(uint8_t);
};

extern struct ADConverter ADConverter;
