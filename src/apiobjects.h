#pragma once

#include <remote-sensor/api.h>

typedef struct ApiObjects {
    StreamBuffers *streamBuffers;
    Request *request;
    Response *response;
    Parameter **parameter;
    struct BufferApi *bufferManager;
} ApiObjects;

void apiObjectsInitEmpty(ApiObjects *api_objects);
