#include "co2sensor.h"
#include "i2c.h"

///
/// Command to read from the sensor
///
static const uint8_t VZ89TE_CMD_GETSTATUS = 0x0C;
///
/// default I2C adress
///
static const uint8_t VZ89TE_ADDR = 0x70;
///
/// \brief Connects to the Sensor via I²C and retrieves the data
/// \param co2 stores the result of the co2 value
/// \param tvoc stores the result for the tvoc value
///
bool readCo2Sensor(uint16_t *co2, uint16_t *tvoc){
  uint8_t buff[7];
  memset(buff,0,sizeof(buff));
  //start I2C communication
  I2C.startWait((VZ89TE_ADDR << 1) + I2C_WRITE);
      //send cmd byte
  I2C.write(VZ89TE_CMD_GETSTATUS);
      //send mandatory data bytes
      for(int i = 0; i < 4; i++) I2C.write(0x00);
      //send crc byte
      I2C.write(0xF3);
      I2C.stop();
  I2C.repStart((VZ89TE_ADDR << 1) + I2C_READ);
  //load all values into buffer
  for(uint8_t i=0; i<7; i++){
      if(i==6)buff[i] = I2C.readNak();
      else buff[i] = I2C.readAck();
  }
  I2C.stop();
  //reset values
  *tvoc = 0;
  *co2 = 0;


    //only first two bytes are relevant for us
    //buff[0] -> tvoc
    //buff[1] -> co2equiv
    //check for valuerange
    //if(buff[0] < 13 || buff[1] < 13 || buff[0] > 242 || buff[1] > 242) return false;
    *tvoc = (buff[0] - 13) * (1000.0/229);
    *co2 = (buff[1] - 13) * (1600.0/229) + 400;

    return true;
}

///
/// \brief handles the request and initiates measurement of co2 value
///
void readCo2SensorHandle(StreamBuffers *streamBuffers, Request *request, Parameter *parameter[], Response *response){
    uint16_t co2, tvoc;
    //get the values
    bool success = readCo2Sensor(&co2, &tvoc);

    if(!BufferManager.buffer_can_fit(4, streamBuffers->output) || !success) response->type = ERROR_FOUND;
    else {
        //push low byte and high byte of both values into the output buffer
        BufferManager.push('C', streamBuffers->output);
        BufferManager.push('0', streamBuffers->output);
        BufferManager.push(tvoc & 0xFF, streamBuffers->output);
        BufferManager.push(tvoc >> 8, streamBuffers->output);
        BufferManager.push(co2 & 0xFF, streamBuffers->output);
        BufferManager.push(co2 >> 8, streamBuffers->output);

        response->type = ACK;
        //fill response
        response->word_size = sizeof(uint16_t);
        response->content_type = UNSIGNED_TYPE;
        response->data = streamBuffers->output->data;
    }
}
