#pragma once

#include <inttypes.h>
#include <remote-sensor/api.h>

static const unsigned char highAddressCMD_STATUS_REGISTERS = 0x10;
static const unsigned char lowAddressSTATE = 0x00;
static const unsigned char lowAddressAXIS_STATE = 0x01;
static const unsigned char lowAddressEVENT = 0x02;
static const unsigned char lowAddressMODE = 0x03;
static const unsigned char lowAddressCTRL = 0x04;
static const unsigned char lowAddressCLEAR_COMMAND = 0x05;
static const unsigned char CLEAR_COMMAND = 0x0F;
static const unsigned char NORMAL_MODE_STDBY = 0x00;
static const unsigned char NORMAL_MODE = 0x01;
static const unsigned char INITIAL_INST_MODE = 0x02;
static const unsigned char OFFSET_AQU_MODE = 0x03;
static const unsigned char SELF_DIAGN_MODE = 0x04;
static const unsigned char CTRL_AXIS_YZ = 0x00;
static const unsigned char CTRL_AXIS_XZ = 0x01;
static const unsigned char CTRL_AXIS_XY = 0x02;
static const unsigned char CTRL_AXIS_AUTO_SWITCH = 0x03;
static const unsigned char CTRL_AXIS_INST_SWITCH = 0x04;

typedef struct omrond7sData {
  /*Earthquake-Related Data   0x 20 00 */
  uint8_t MAIN_SI_H;
  uint8_t MAIN_SI_L;
  uint8_t MAIN_PGA_H;
  uint8_t MAIN_PGA_L;

} omrond7sData;

void initOmron(void);

uint8_t earthquakeFlag(void);

uint8_t getCurrentStatusOmron(void);

uint8_t getCurrentAxisStateOmron(void);

uint8_t getCurrentEventsOmron(void);

uint8_t getCurrentModeOmron(void);

uint8_t setCurrentModeOmron(uint8_t mode);

uint8_t getCurrentCTRLOmron(void);

uint8_t setCurrentCTRLOmron(uint8_t ctrl);

uint8_t clearOmronRegisters(void);

struct omrond7sData *getLatestOmronValues (void);

struct omrond7sData *getSelfDiagnosticDataOmron(void);

void setLatestOmronResponse(StreamBuffers *, struct BufferApi *, Response *);

#ifndef LOCAL_TESTS
void readOmronD7sHandle(StreamBuffers *, Request *, Parameter *[], Response *);
#else
void readOmronD7sHandle(StreamBuffers *, Request *, Parameter *[], Response *, struct BufferApi *);
#endif

void createNewBufferForOmronD7s(StreamBuffers *, struct BufferApi *);
	
uint16_t mergeTwoBytes(uint8_t highByte, uint8_t lowByte);

float castToFloat(uint8_t highByte, uint8_t lowByte);

float castTempToFloat(uint8_t highByte, uint8_t lowByte);
