/**
 * File		adapter_BME680.c
 * @date	20 Mai 2018
 * @version	1.0
 *
 */

/*! @file adapter_bme680.c
 *  @brief Adapter for BME680 sensor API
 */


/* Header includes */
#include <bme680.h>

#ifdef __AVR__
#include <util/delay.h>
#else
#include <unistd.h>
#endif // __AVR__

/* Included when self test macro is defined */
#ifdef BME680_SELFTEST
#include <Self test/bme680_selftest.h>
#endif

#include "i2c.h"
#include "adapter_BME680.h"

/* Function prototypes */
/*!
 * @fn user_delay_ms
 * @brief This internal API provides the platform dependend delay functionality.
 *
 * @note Required as used in the BME680 API (e.g. sensor read) and passed by pointer to the adapter device.
 *
 * @param period	: 32bit integer. Defined in sensor settings and provided by sensor device struct.
 *
 */
void user_delay_ms(uint32_t period);

/*!
 * @fn user_i2c_read
 * @brief This internal API provides the platform dependend TWI read function.
 *
 * @note Required as used in the BME680 API (e.g. sensor read) and passed by pointer to the adapter device.
 *
 * @see I2C
 *
 * @param dev_id	:Structure instance of bme680_dev.
 * @param reg_addr	:Contains the register address array in which the data will be read.
 * @param reg_addr	:Contains the register data array in which the data will be read.
 * @param reg_addr	:Contains the register data length.
 *
 * @return Result of API execution status
 * @retval zero -> Success / +ve value -> Warning / -ve value -> Error
 */
unsigned char user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);

/*!
 * @fn user_i2c_write
 * @brief This internal API provides the platform dependend TWI write function.
 *
 * @note Required as used in the BME680 API (e.g. sensor read) and passed by pointer to the adapter device.
 *
 * @see I2C
 *
 * @param dev_id	:Structure instance of bme680_dev.
 * @param reg_addr	:Contains the register address array to which the data will be written.
 * @param reg_addr	:Contains the register data array to which the data will be written.
 * @param reg_addr	:Contains the register data length.
 *
 * @return Result of API execution status
 * @retval zero -> Success / +ve value -> Warning / -ve value -> Error
 */
unsigned char user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);

/*!
 * @fn initialize_BME680
 * @brief This internal API initializes the BME680 device. This function is internally called by the create_and _initialize_BME680 function.
 * The provided arguments sets the sensor to the desired mode. Only two parameters can be changed as the sensor init is implemented else
 * with predefined values according to the BME680 manual.
 * The slave address can be changed between two fixed addresses and the gas sensor can be enabled or disabled.
 *
 * @see create_and_initalize_BME680_device
 *
 * The possible slave addresses of the BME680 hardware.
 *  value  | Description
 * --------|--------------
 *   0x76     | BME680_I2C_ADDR_PRIMARY
 *   0x77     | BME680_I2C_ADDR_SECONDARY
 *
 * The options to enable or disable the gas measurement.
 *  value  | Description
 * --------|--------------
 *   0     | BME680_GAS_SENSOR_DISABLED
 *   1     | BME680_GAS_SENSOR_ENABLED
 *
 * @param BME680_SLAVE_ADDRESS	: The slave address of the BME680 hardware.
 * @param BME680_GAS_SENSOR_USE	: The gas sensor mode.
 *
 * @return Result of API execution status
 * @retval zero -> Success / +ve value -> Warning / -ve value -> Error
 */
int8_t initialize_BME680(uint8_t BME680_SLAVE_ADDRESS, uint8_t BME680_GAS_SENSOR_USE);

/*!
 * @fn configure_BME680
 * @brief This internal API configures the BME680 device. This function is internally called by the create_and _initialize_BME680 function.
 * The provided argument sets the sensor to the desired mode. Only one parameter can be changed as the sensor init is implemented else
 * with predefined values according to the BME680 manual.
 * The gas sensor can be enabled or disabled.
 *
 * @see create_and_initalize_BME680_device
 *
 * The options to enable or disable the gas measurement.
 *  value  | Description
 * --------|--------------
 *   0     | BME680_GAS_SENSOR_DISABLED
 *   1     | BME680_GAS_SENSOR_ENABLED
 *
 * @param BME680_GAS_SENSOR_USE	: The gas sensor mode.
 *
 * @return Result of API execution status
 * @retval zero -> Success / +ve value -> Warning / -ve value -> Error
 */
int8_t configure_BME680(uint8_t BME680_GAS_SENSOR_USE);

/*!
 * @fn createNewBufferForBME680Sensor
 * @brief This internal API creates a new buffer with size of BME680_READINGS_SIZE.
 * The buffer delivers the sensor readings to the RSApi.
 *
 * @see RemoteSensorAPI
 *
 * @param streamBuffers	: The buffer to store the readings for RSApi.
 * @param bufferManager	: The buffer manager to handle buffer functionality.
 *
 */
void createNewBufferForBME680Sensor(StreamBuffers *streamBuffers, struct BufferApi *bufferManager);

/* structure definitions */
/*!
 * @struct dev
 * @brief Structure instance of bme680_dev. Contains the settings and function pointers
 * required to initialize and interface the BME680 sensor.
 *
 * @see bme680_dev
 *
 */
static struct bme680_dev dev;

/*!
 * @struct data
 * @brief Structure instance of bme680_field_data. Contains the readings from the sensor.
 *
 * @see bme680_field_data
 *
 */
static struct bme680_field_data data;

/* Included when Remote Sensor API is used with registration mode (define REMOTE_SENSOR_REGISTRATION macro) */
#ifdef REMOTE_SENSOR_REGISTRATION
/*!
 * @var BME680_READINGS_SIZE
 * @brief This value is required by the get_sensor_data_from_buffer_BME680 function
 * and set by the initialize_BME680 function.
 * It sets the number of bytes to read into the stream buffer and is determined by the
 * BME680_FLOAT_POINT_COMPENSATION macro and the BME680_GAS_SENSOR_DISABLED macro.
 *
 * @see initialize_BME680
 * @see get_sensor_data_from_buffer_BME680
 * @see BME680_GAS_SENSOR_DISABLED
 * @see BME680_FLOAT_POINT_COMPENSATION
 *
 */
static uint8_t BME680_READINGS_SIZE;
#endif

/****************** Global Function Definitions *******************************/
/*!
 *@brief This internal API provides the platform dependend delay functionality.
 */
void user_delay_ms(uint32_t period)
{
#ifdef __AVR__
  while (period > 0){
    _delay_ms(1); /* _delay_ms must be called with const integer argument */
    period--;
  }
#else
    sleep(period);
#endif //__AVR__
}

/*!
 *
 * @brief This internal API provides the platform dependend TWI write function.
 *
 */
unsigned char user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len)
{
    unsigned char rslt = 0; /* Return 0 for Success, non-zero for failure */
    I2C.startWait((unsigned char)((dev_id << 1) + I2C_WRITE));     /* Set device address and write mode */
    rslt = I2C.write(reg_addr);
    rslt = I2C.repStart((unsigned char)((dev_id << 1) + I2C_READ));  /* Set device address and read mode */
    /* Read from #len registers, increase the address #len times */
    for(uint16_t i = 0; i < len; i++){
        if(i < len -1){
            *reg_data = I2C.readAck();
        }else{
            *reg_data = I2C.readNak(); /* After the last read send NACK */
        }
        reg_data++;
    }
    I2C.stop();
    return rslt;
}

/*!
 *
 * @brief This internal API provides the platform dependend TWI read function.
 *
 */
unsigned char user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len)
{
    unsigned char rslt = 0; /* Return 0 for Success, non-zero for failure */
    I2C.startWait((unsigned char)((dev_id << 1) + I2C_WRITE)); /* Set device address and write mode */
    rslt = I2C.write(reg_addr);
    if(rslt != 0) return rslt;
    /* Write to #len registers, increase the address #len times */
    for(uint16_t i = 0; i < len; i++){
        rslt = I2C.write(*reg_data);
            if(rslt != 0) return rslt;
            reg_data++;
    }
    I2C.stop();
    return rslt;
}

/*!
 *
 * @brief The function instanciates the device interfaces and configures as well as initializes the BME680 sensor.
 *
 */
int8_t create_and_initalize_BME680_device(uint8_t BME680_SLAVE_ADDRESS, uint8_t BME680_GAS_SENSOR_USE)
{
    int8_t rslt = BME680_OK;
    rslt = initialize_BME680(BME680_SLAVE_ADDRESS, BME680_GAS_SENSOR_USE);
    if(rslt != 0) return rslt;
    rslt = configure_BME680(BME680_GAS_SENSOR_USE);
    return rslt;
}

/*!
 *
 * @brief This internal API initializes the BME680 device.
 *
 */
int8_t initialize_BME680(uint8_t BME680_SLAVE_ADDRESS, uint8_t BME680_GAS_SENSOR_USE)
{
/* This code is redundant without Remote Sensor API use. To keep the flash memory consumption minimal */
#ifdef REMOTE_SENSOR_REGISTRATION
#ifndef BME680_FLOAT_POINT_COMPENSATION
       /* Without gas sensor 10 bytes(2x uint32_t + 1x uint16_t) will be read and two identifier bytes are added */
       if(BME680_GAS_SENSOR_USE == BME680_GAS_SENSOR_DISABLED) BME680_READINGS_SIZE = 0x0C;
       /* With gas sensor 14 bytes(3x uint32_t + 1x uint16_t) will be read and two identifier bytes are added */
       else BME680_READINGS_SIZE = 0x10;
#else
        /* Without gas sensor 12 bytes (3 floats) will be read and two identifier bytes are added */
        if(BME680_GAS_SENSOR_USE == BME680_GAS_SENSOR_DISABLED) BME680_READINGS_SIZE = 0x0E;
        /* With gas sensor 16 bytes (4 floats) will be read and two identifier bytes are added */
        else BME680_READINGS_SIZE = 0x12;
#endif
#endif
        dev.dev_id = BME680_SLAVE_ADDRESS; /* Set the slave address */
        dev.intf = BME680_I2C_INTF; /* Set TWI communication */
        /* Provide the platform dependent functions to the Bosch sensor API */
        dev.read = (bme680_com_fptr_t) user_i2c_read;
        dev.write = (bme680_com_fptr_t) user_i2c_write;
        dev.delay_ms = (bme680_delay_fptr_t) user_delay_ms;
        /* amb_temp can be set to 25 prior to configuring the gas sensor
        * or by performing a few temperature readings without operating the gas sensor.
        */
        dev.amb_temp = 25;
        int8_t rslt = BME680_OK;
        rslt = bme680_init(&dev); /* Call the Bosch API init function */
        return rslt;
}

/*!
 *
 * @brief This internal API configures the BME680 device.
 *
 */
int8_t configure_BME680(uint8_t BME680_GAS_SENSOR_USE)
{
        int8_t rslt = BME680_OK;
        uint8_t set_required_settings;

	    /* Set the temperature, pressure and humidity settings */
	dev.tph_sett.os_hum = BME680_OS_2X; //humidity oversampling
	dev.tph_sett.os_pres = BME680_OS_4X; //pressure oversampling
	dev.tph_sett.os_temp = BME680_OS_8X; //temp oversampling
	dev.tph_sett.filter = BME680_FILTER_SIZE_3; //IIR filter setting

	    /* Set the remaining gas sensor settings and link the heating profile */
	if(BME680_GAS_SENSOR_USE == BME680_GAS_SENSOR_ENABLED)
	{
	  dev.gas_sett.run_gas = BME680_ENABLE_GAS_MEAS;
	  /* Create a ramp heat waveform in 3 steps */
	  dev.gas_sett.heatr_temp = 320; /* degree Celsius */
	  dev.gas_sett.heatr_dur = 150; /* milliseconds */
	}else{
	  dev.gas_sett.run_gas = BME680_DISABLE_GAS_MEAS;
	}
	    /* Select the power mode */
	    /* Must be set before writing the sensor configuration */
	dev.power_mode = BME680_FORCED_MODE;

	    /* Set the required sensor settings needed */
	if(BME680_GAS_SENSOR_USE == BME680_GAS_SENSOR_ENABLED)
	{
	    set_required_settings = BME680_OST_SEL | BME680_OSP_SEL | BME680_OSH_SEL | BME680_FILTER_SEL | BME680_GAS_SENSOR_SEL; /* Activate gas sensor */
	}else{
	    set_required_settings = BME680_OST_SEL | BME680_OSP_SEL | BME680_OSH_SEL | BME680_FILTER_SEL; /* Don't activate gas sensor */
	}
	    /* Set the desired sensor configuration */
	rslt = bme680_set_sensor_settings(set_required_settings,&dev);

	    /* Set the power mode */
	rslt = bme680_set_sensor_mode(&dev);
	    return rslt;
}

/*!
 *
 * @brief This internal API reads the BME680 measurements into the internal data_field structure which is initialized in this API.
 *
 */
int8_t read_sensor_data_BME680(void)
{
    int8_t rslt = BME680_OK;
    /* Get the total measurement duration so as to sleep or wait till the
     * measurement is complete */
    uint16_t meas_period;
    bme680_get_profile_dur(&meas_period, &dev);
    user_delay_ms(meas_period); /* Delay till the measurement is ready */
    rslt = bme680_get_sensor_data(&data, &dev);
    /* Trigger the next measurement if you would like to read data out continuously */
    if (dev.power_mode == BME680_FORCED_MODE) {
        rslt = bme680_set_sensor_mode(&dev);
    }
    return rslt;
}


#ifdef BME680_SELFTEST
/*!
 *
 * @brief  Accesses the Self-test API for the BME680.
 *
 */
int8_t selftest_BME680(void){
  return bme680_self_test(&dev); /* Call the Bosch API self test function */
}
#endif

/* This code is redundant without Remote Sensor API use. To keep the flash memory consumption minimal */
#ifdef REMOTE_SENSOR_REGISTRATION
/*!
 *
 * @brief This internal API reads the data from the internal data buffer into the provided stream buffer.
 *
 */
bool get_sensor_data_from_buffer_BME680(ApiObjects *apiObject){
  /* Check if data fits into buffer, else destroy and create new one with fitting size*/
  if (!apiObject->bufferManager->buffer_can_fit(BME680_READINGS_SIZE, apiObject->streamBuffers->output))
      createNewBufferForBME680Sensor(apiObject->streamBuffers, apiObject->bufferManager);

  /* Push the identifier for the gateway into the buffer first */
  apiObject->bufferManager->push(BME680_FIRST_HEADER_BYTE, apiObject->streamBuffers->output);
  apiObject->bufferManager->push(BME680_SECOND_HEADER_BYTE, apiObject->streamBuffers->output);

/* Data are stored as integers */
#ifndef BME680_FLOAT_POINT_COMPENSATION
  /* The pointer is allocated to the addresses of the sensors data field structure and used to break the data into bytes */
  static uint8_t *convert_integer_reading_ptr = NULL;
  /* The data are read bytewise into the buffer in the following sequence (starting with temperature) */
  for(uint8_t i = 0; i < 4; i++){
      switch (i) {
        case 1: convert_integer_reading_ptr = (uint8_t*) &data.humidity;
          break;
        case 2: convert_integer_reading_ptr = (uint8_t*) &data.pressure;
          break;
        case 3: convert_integer_reading_ptr = (uint8_t*) &data.gas_resistance;
          break;
        default: convert_integer_reading_ptr = (uint8_t*) &data.temperature;
        }
      /* Loop over the number of bytes in the sensors data field structure (max uint32_t) */
      for(uint8_t j = 0; j < 4; j++){
          /* Break loop if two bytes of uint16_t temperature has been read or if gas sensor is disabled */
          if((i == 0 && j == 2) || (i == 4 && BME680_READINGS_SIZE == 0x0C)) break;
          /* Push the data bytes into the buffer */
          if (convert_integer_reading_ptr[j] != apiObject->bufferManager->push(convert_integer_reading_ptr[j], apiObject->streamBuffers->output))
              return false;
      }
#else
  /* Data are stored as float */
  /* The pointer is allocated to the addresses of the sensors data field structure and used to break the data into bytes */
  static uint8_t *convert_float_reading_ptr = NULL;
   /* The data are read bytewise into the buffer in the following sequence (starting with temperature) */
  for(uint8_t i = 0; i < 4; i++){
      switch (i) {
        case 1: convert_float_reading_ptr = (uint8_t*) &data.humidity;
          break;
        case 2: convert_float_reading_ptr = (uint8_t*) &data.pressure;
          break;
        case 3: convert_float_reading_ptr = (uint8_t*) &data.gas_resistance;
          break;
        default: convert_float_reading_ptr = (uint8_t*) &data.temperature;
        }
      /* Loop over the number of bytes in the sensors data field structure (all floats) */
      for(uint8_t j = 0; j < 4; j++){
          /* Break loop if gas sensor is disabled */
          if (i == 4 && BME680_READINGS_SIZE == 0x0E) break;
          /* Push the data bytes into the buffer */
          if (convert_float_reading_ptr[j] != apiObject->bufferManager->push(convert_float_reading_ptr[j], apiObject->streamBuffers->output))
              return false;
      }
#endif
  }

  /* Fill Remote Sensor API response */
  apiObject->response->word_size = sizeof(uint8_t);
  apiObject->response->content_type = UNSIGNED_TYPE;
  return  true;
}

/*!
 *
 * @brief This API is the interface to the Remote Sensor API. The sensor will be read
 * and the readings will be passed to the buffer when this function is called.
 *
 */
void readBME680Handle(StreamBuffers *streamBuffers, Request *request,
                          Parameter *parameter[], Response *response)
{
    ApiObjects apiObjects = {
        .streamBuffers = streamBuffers,
        .request = request,
        .response = response,
        .parameter = parameter,
        .bufferManager = &BufferManager,
    };
    /* Read sensor by TWI*/
    read_sensor_data_BME680();
    /* Get read data from data field structure */
    get_sensor_data_from_buffer_BME680(&apiObjects);
}

/*!
 *
 * @brief This internal API creates a new buffer with size of BME680_READINGS_SIZE.
 *
 */
void createNewBufferForBME680Sensor(StreamBuffers *streamBuffers, struct BufferApi *bufferManager)
{
    bufferManager->destroy(streamBuffers->output);
    streamBuffers->output = bufferManager->create(BME680_READINGS_SIZE);
}
#endif

/*!
 *
 * @brief Returns the pointer to the instance of the BME680 device structure.
 *
 */
const struct bme680_dev *return_device_ptr_BME680(void){
  return &dev;
}

/*!
 *
 * @brief Returns the pointer to the instance of the BME680 field_data structure.
 *
 */
const struct bme680_field_data *return_data_field_ptr_BME680(void){
  return &data;
}

