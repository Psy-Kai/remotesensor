#if defined (__AVR__)
#include <stdio.h>
#include <i2c.h>
#include <adconverter.h>
#include <windsensor.h>
#include <apds_9250.h>
#include <veml6075.h>
#include <co2sensor.h>
#include <adapter_BME680.h>
#include <bme680.h>
#include <omron_D7.h>
#if defined(__AVR__)
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <logger.h>
#endif // __AVR__

void printD7Svalues(void);
void printADCValues(ADConverterValue ADCvalue);
void print_apds_data(struct apds9250Data *apds_ptr);
void printCo2sensor(void);
void printBMEdata(const struct bme680_field_data *data);
void printUvValues(struct veml6075Data *data);
void initSystem(void);

static char* msg = NULL;
static char print[128];


void initSystem(void)
{
#if defined(__AVR__)
    ADConverter.init();
    I2C.init();
#endif // __AVR__
    initAPDS9250Sensor();
    initUVSensor();
    create_and_initalize_BME680_device(BME680_I2C_ADDR_SECONDARY, BME680_GAS_SENSOR_ENABLED);
}


int main(int argc, char *argv[])
{
   (void)argc;
   (void)argv;

    Logger.init(9600,0,DEBUG);

    msg = "Testing sensors module IoT Project SS2018\r\n";
    Logger.log(msg,DEBUG);

    initSystem();

    struct apds9250Data apdsData;
    const struct bme680_field_data *bmeData = return_data_field_ptr_BME680();
    struct veml6075Data UVData;

    while (true) {
        msg = "Sensor data:\r\n";
        Logger.log(msg,DEBUG);

        getAPDS9250Values(&apdsData);
        print_apds_data(&apdsData);

        printCo2sensor();

        read_sensor_data_BME680();
        printBMEdata(bmeData);

        getUvValues(&UVData);
        printUvValues(&UVData);

        printADCValues(ADConverter.read(ADC_Pin8));

        printD7Svalues();

        msg = "End sensor data\r\n\r\n";
        Logger.log(msg,DEBUG);

        _delay_ms(5000);

    }

}

void printD7Svalues(void){
    getCurrentEventsOmron();
    struct omrond7sData *data = getLatestOmronValues();
    sprintf(print, "MAIN_SI: %.2f, MAIN_PGA: %.2f\r\n", castToFloat(data->MAIN_SI_H, data->MAIN_SI_L),
            castToFloat(data->MAIN_PGA_H, data->MAIN_PGA_L));
    Logger.log(print, DEBUG);
    clearOmronRegisters();
}

void printADCValues(ADConverterValue ADCvalue){
  sprintf(print, "ADC Value: %u\r\n", ADCvalue.value);
  Logger.log(print, DEBUG);
}

//concat chunk of values into 16bit integer and print it
void printUvValues(struct veml6075Data *data){
    uint16_t UVA_Data, UVB_Data, UVC1_Data, UVC2_Data = 0;
    UVA_Data = (uint16_t) ((data->uva_high << 8) | data->uva_low);
    UVB_Data = (uint16_t) ((data->uvb_high << 8) | data->uvb_low);
    UVC1_Data = (uint16_t) ((data->uvcomp1_high << 8) | data->uvcomp1_low);
    UVC2_Data = (uint16_t) ((data->uvcomp2_high << 8) | data->uvcomp2_low);
    sprintf(print,"UVA: %u, UVB: %u, UVComp1: %u, UVComp2: %u\r\n",UVA_Data,UVB_Data,UVC1_Data,UVC2_Data);
    Logger.log(print, DEBUG);
}

void printBMEdata(const struct bme680_field_data *data){
#ifndef BME680_FLOAT_POINT_COMPENSATION
  if(data->status & BME680_GASM_VALID_MSK){
      sprintf(print, "T: %u degC, P: %lu hPa, H: %lu %%rH, G: %lu ohms\r\n",
              data->temperature / 100, data->pressure / 100, data->humidity/1000,  data->gas_resistance);

  }else{
      sprintf(print, "T: %u degC, P: %lu hPa, H: %lu %%rH",
              data->temperature / 100, data->pressure / 100, data->humidity/1000);
  }
 Logger.log(print, DEBUG);
#else
  if(data->status & BME680_GASM_VALID_MSK){
      sprintf(print, "BME data T: %.2f degC, P: %.2f hPa, H %.2f %%rH, G: %.2f ohms\r\n",
              data->temperature, data->pressure / 100.0f, data->humidity, data->gas_resistance);

    }else{
      sprintf(print, "BME data T: %.2f degC, P: %.2f hPa, H %.2f %%rH\r\n",
              data->temperature, data->pressure / 100.0f, data->humidity);
    }
  Logger.log(print, DEBUG);
#endif
}

void printCo2sensor(void){
  uint16_t co2, tvoc = 0;
  readCo2Sensor(&co2, &tvoc);
  sprintf(print, "CO2: %u, TVOC: %u\r\n", co2, tvoc);
  Logger.log(print, DEBUG);
}

//concat chunk of values in 32bit integer and print it
void print_apds_data(struct apds9250Data *apds_ptr)
{
  uint8_t *valptr = malloc(sizeof(uint32_t));
  *valptr = apds_ptr->w_low;
  valptr++;
  *valptr = apds_ptr->w_med;
  valptr++;
  *valptr = apds_ptr->w_high;
  valptr++;
  *valptr = 0x00;
  valptr -= 3;
  uint32_t *tmp = (uint32_t*)valptr;

  sprintf(print, "White value: %lu", *tmp);
  Logger.log(print, DEBUG);

  *valptr = apds_ptr->b_low;
  valptr++;
  *valptr = apds_ptr->b_med;
  valptr++;
  *valptr = apds_ptr->b_high;
  valptr++;
  *valptr = 0x00;
  valptr -= 3;

  sprintf(print, "Blue value: %lu", *tmp);
  Logger.log(print, DEBUG);

  *valptr = apds_ptr->g_low;
  valptr++;
  *valptr = apds_ptr->g_med;
  valptr++;
  *valptr = apds_ptr->g_high;
  valptr++;
  *valptr = 0x00;
  valptr -= 3;

  sprintf(print, "Green value: %lu", *tmp);
  Logger.log(print, DEBUG);

  *valptr = apds_ptr->r_low;
  valptr++;
  *valptr = apds_ptr->r_med;
  valptr++;
  *valptr = apds_ptr->r_high;
  valptr++;
  *valptr = 0x00;
  valptr -= 3;

  sprintf(print, "Red value: %lu\r\n", *tmp);
  Logger.log(print, DEBUG);

  free(valptr);
  free(tmp);
  valptr = NULL;
  tmp = NULL;
}
#endif //AVR
