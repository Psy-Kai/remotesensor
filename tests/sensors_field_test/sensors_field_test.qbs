import qbs

CppApplication {
    name: "Sensors Field Test"

    files: [
        "sensors_field_test.c",
    ]

    Properties {
        condition: avr.flash && qbs.architecture.startsWith("avr")
        type: "flash_to_board"
    }

    Depends {
        name: "avr"
    }
    Depends {
        name: "Sensors"
    }

}
