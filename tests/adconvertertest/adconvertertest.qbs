import qbs

CppApplication {
    name: "ADConverter Test"

    files: [
        "adconvertertest.c",
    ]

    Depends {
        name: "avr"
    }
    Depends {
        name: "Unity"
    }
    Depends {
        name: "Sensors"
    }
}
