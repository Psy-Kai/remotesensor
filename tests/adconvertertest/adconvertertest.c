#include <unity.h>
#include <adconverter.h>

#define ADCONVERTER_VALUE_HIGH ((uint8_t)0xF0)
#define ADCONVERTER_VALUE_LOW ((uint8_t)0x0F)
#define ADCONVERTER_VALUE ((uint16_t)((ADCONVERTER_VALUE_HIGH << 8) + ADCONVERTER_VALUE_LOW))

ADConverterValue mockAdConverterRead(uint8_t pin)
{
    ADConverterValue result = { ADCONVERTER_VALUE };
    return result;
    (void)pin;
}

void testAdConverterResult()
{
    struct ADConverter ad_converter;
    ad_converter.read = &mockAdConverterRead;
    ADConverterValue value = ad_converter.read(0);

    TEST_ASSERT_EQUAL_UINT16(value.value, ADCONVERTER_VALUE);
    TEST_ASSERT_EQUAL_UINT8(value.valueArray[1], ADCONVERTER_VALUE_HIGH);
    TEST_ASSERT_EQUAL_UINT8(value.valueArray[0], ADCONVERTER_VALUE_LOW);
}

int main()
{
    UNITY_BEGIN();

    RUN_TEST(testAdConverterResult);

    return UNITY_END();
}
