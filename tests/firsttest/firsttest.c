#include <unity.h>
#include <someexamplefunctions.h>

void testInitResourceManager()
{
    TEST_ASSERT_FALSE(initResourceManager(NULL));
    TEST_ASSERT_TRUE(initResourceManager(&ResourceManager));
}

void testInitCommunicoationController()
{
    TEST_ASSERT_FALSE(initCommunicationController(NULL));
    TEST_ASSERT_TRUE(initCommunicationController(&CommunicationController));
}

int main()
{
    UNITY_BEGIN();

    RUN_TEST(testInitResourceManager);
    RUN_TEST(testInitCommunicoationController);

    return UNITY_END();
}
