import qbs

CppApplication {
    name: "First Tests"

    files: [
        "firsttest.c",
    ]

    Depends {
        name: "avr"
    }
    Depends {
        name: "Unity"
    }
    Depends {
        name: "Sensors"
    }
}
