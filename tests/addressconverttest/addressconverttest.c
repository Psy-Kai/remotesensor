#include <unity.h>
#include <802154adapter.h>

void testConvertShortAddress()
{
    uint8_t addrArray[] = { 0xAA, 0xBB };
    TEST_ASSERT_EQUAL_HEX16(0xAABB, addressArrayToInt16(addrArray));
}

void testConvertExtendedAddress()
{
    uint8_t addrArray[] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88 };
    TEST_ASSERT_EQUAL_HEX64(0x1122334455667788, addressArrayToInt64(addrArray));
}

int main()
{
    UNITY_BEGIN();

    RUN_TEST(testConvertShortAddress);
    RUN_TEST(testConvertExtendedAddress);

    return UNITY_END();
}
