import qbs

CppApplication {
    name: "Address Convert Tests"

    files: [
        "addressconverttest.c",
    ]

    Depends {
        name: "avr"
        required: false
    }
    Depends {
        name: "Unity"
    }
    Depends {
        name: "Remote Sensor"
    }
}
