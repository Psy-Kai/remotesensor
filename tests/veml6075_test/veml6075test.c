#include <unity.h>
#include "veml6075.h"
#include "apiobjects.h"
#include <i2c.h>
/*
extern "C" {
 since some functions in the remote sensor api are called
   "delete" we need to rename them in c++
#define delete extern_delete
#include "veml6075.h"
#undef delete
}
*/

char i2cReadWriteFlow;
unsigned char i2cWriteMockByte = 0;
bool i2cIsStoped;
unsigned char UVA_DATA_REG = 0x07;
unsigned char UVB_DATA_REG = 0x09;
unsigned char UVCOMP1_DATA_REG = 0x0A;
unsigned char UVCOMP2_DATA_REG = 0x0B;

uint8_t mockBufferManagerBufferCanFit(size_t size, Buffer *buffer)
{
    TEST_ASSERT_NOT_NULL(buffer);
    if ((buffer->count + size) <= buffer->size)
        return 1;
    return 0;
}

uint8_t mockBufferManagerBufferCanFitFailed(size_t size, Buffer *buffer)
{
    (void)size;

    TEST_ASSERT_NOT_NULL(buffer);
    return 0;


}

uint8_t mockBufferManagerPush(uint8_t value, Buffer *buffer)
{
    if (pow(256, sizeof(uint8_t)) <= buffer->count)
        return value + 1;
    if (buffer->size <= buffer->count)
        return value + 1;

    buffer->data[buffer->count] = value;
    ++(buffer->count);
    return value;
}

uint8_t mockBufferManagerPushFailed(uint8_t value, Buffer *buffer)
{
    (void)buffer;

    return value + 1;

}

void initBufferManager(struct BufferApi *buffer_manager)
{
    buffer_manager->buffer_can_fit = &mockBufferManagerBufferCanFit;
    buffer_manager->push = &mockBufferManagerPush;
}

void deinitBufferManager(struct BufferApi *buffer_manager)
{
    (void)buffer_manager;
}

void initBuffer(Buffer *buffer)
{
    buffer->count = 0;
    buffer->size = sizeof(uint16_t);
    buffer->data = buffer->head = malloc(buffer->size);
    buffer->end = buffer->tail = buffer->data + buffer->size;
}

Buffer* createBuffer(uint8_t num)
{
    Buffer* buffer = (Buffer*)malloc(sizeof(Buffer));
    buffer->count = 0;
    buffer->size = sizeof(uint8_t)*num;
    buffer->data = buffer->head = malloc(buffer->size);
    buffer->end = buffer->tail = buffer->data + buffer->size;
    return buffer;
}

void deinitBuffer(Buffer *buffer)
{
    free(buffer->data);
}

void initStreamBuffers(StreamBuffers *stream_buffers)
{
    stream_buffers->output = (Buffer*)malloc(sizeof(Buffer));
    initBuffer(stream_buffers->output);
}

void deinitStreamBuffers(StreamBuffers *stream_buffers)
{
    deinitBuffer(stream_buffers->output);
    free(stream_buffers->output);
}

void initApiObjects(struct ApiObjects *api_objects)
{
    apiObjectsInitEmpty(api_objects);
    api_objects->bufferManager = (struct BufferApi*)malloc(sizeof(struct BufferApi));
    initBufferManager(api_objects->bufferManager);
    api_objects->streamBuffers = (StreamBuffers*)malloc(sizeof(StreamBuffers));
    initStreamBuffers(api_objects->streamBuffers);
    api_objects->response = (Response*)malloc(sizeof(Response));
}

void deinitApiObjects(struct ApiObjects *api_objects)
{
    free(api_objects->response);
    deinitStreamBuffers(api_objects->streamBuffers);
    free(api_objects->streamBuffers);
    deinitBufferManager(api_objects->bufferManager);
    free(api_objects->bufferManager);
}

void i2cInitMock() { }

void i2cStopMock() { i2cIsStoped=true; }

unsigned char i2cStartMock(unsigned char address)
{
    i2cIsStoped=false;
    if(address>0) return 0;
    else return 1;
}

unsigned char i2cRepStartMock(unsigned char address)
{
    i2cIsStoped=false;
    if(address % 2 == 1)
    {
        i2cReadWriteFlow = 'R';
    }
    else i2cReadWriteFlow='W';
    if(address>0) return 0;
    else return 1;
}

void i2cStartWaitMock(unsigned char address)
{
    if(address>0) i2cIsStoped=false;
}

unsigned char i2cWriteMock(unsigned char data)
{
    i2cWriteMockByte = data;
    if(data!=0) return 0;
    return 1;
}

unsigned char i2cReadAckMock()
{
    if(i2cWriteMockByte==UVA_DATA_REG) return 0x0f;
    if(i2cWriteMockByte==UVB_DATA_REG) return 0x0f;
    if(i2cWriteMockByte==UVCOMP1_DATA_REG) return 0x0f;
    if(i2cWriteMockByte==UVCOMP2_DATA_REG) return 0x0f;
    return 1;
}

unsigned char i2cReadNakMock()
{
    if(i2cWriteMockByte==UVA_DATA_REG) return 0xf0;
    if(i2cWriteMockByte==UVB_DATA_REG) return 0xf0;
    if(i2cWriteMockByte==UVCOMP1_DATA_REG) return 0xf0;
    if(i2cWriteMockByte==UVCOMP2_DATA_REG) return 0xf0;
    return 1;
}

unsigned char i2cReadMock(unsigned char ack) { return 0xff; }


void testSetResponseBufferTooSmall()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    api_objects.bufferManager->buffer_can_fit = &mockBufferManagerBufferCanFitFailed;
    api_objects.bufferManager->destroy = &deinitBuffer;
    api_objects.bufferManager->create = &createBuffer;
    struct veml6075Data data = {1,1,1,1,1,1,1,1};
    setResponse(&data,api_objects.streamBuffers, api_objects.bufferManager, api_objects.response);
    TEST_ASSERT_EQUAL_UINT8(10, api_objects.streamBuffers->output->count);
    deinitApiObjects(&api_objects);
}

void testSetResponseBufferCannotPush()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    api_objects.bufferManager->push = &mockBufferManagerPushFailed;
    api_objects.bufferManager->destroy = &deinitBuffer;
    api_objects.bufferManager->create = &createBuffer;
    struct veml6075Data data = {1,1,1,1,1,1,1,1};
    setResponse(&data,api_objects.streamBuffers, api_objects.bufferManager, api_objects.response);
    TEST_ASSERT_EQUAL_UINT8(0, api_objects.streamBuffers->output->count);
    deinitApiObjects(&api_objects);
}

void testSetResponseSuccessful()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    api_objects.bufferManager->buffer_can_fit = &mockBufferManagerBufferCanFit;
    api_objects.bufferManager->push = &mockBufferManagerPush;
    api_objects.bufferManager->destroy = &deinitBuffer;
    api_objects.bufferManager->create = &createBuffer;
    struct veml6075Data data = {0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0};
    setResponse(&data,api_objects.streamBuffers, api_objects.bufferManager, api_objects.response);

    TEST_ASSERT_EQUAL(ACK, api_objects.response->type);
    TEST_ASSERT_EQUAL_UINT8(10, api_objects.streamBuffers->output->count);
    TEST_ASSERT_EQUAL_UINT8(sizeof(uint8_t), api_objects.response->word_size);
    TEST_ASSERT_EQUAL(UNSIGNED_TYPE, api_objects.response->content_type);
    deinitApiObjects(&api_objects);
}
/*
void testReadUvSensorHandle()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    struct veml6075Data data={0,0,0,0,0,0,0,0};
    api_objects.bufferManager->buffer_can_fit = &mockBufferManagerBufferCanFit;
    api_objects.bufferManager->push = &mockBufferManagerPush;
    api_objects.bufferManager->destroy = &deinitBuffer;
    api_objects.bufferManager->create = &createBuffer;
    struct I2C i2c;
    initI2C(&i2c);
    i2cWriteMockByte=1;


    readUvSensorHandle(api_objects.streamBuffers, api_objects.request, api_objects.parameter,
                        api_objects.response, &i2c, api_objects.bufferManager, &data);
    TEST_ASSERT_EQUAL(ACK, api_objects.response->type);
    TEST_ASSERT_EQUAL_UINT8(10, api_objects.streamBuffers->output->count);
    TEST_ASSERT_EQUAL_UINT8(10, api_objects.streamBuffers->output->size);
    TEST_ASSERT_EQUAL_UINT8(sizeof(uint8_t), api_objects.response->word_size);
    TEST_ASSERT_EQUAL(UNSIGNED_TYPE, api_objects.response->content_type);
    TEST_ASSERT_EQUAL_UINT8(0xF0, data.uva_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.uva_low);
    TEST_ASSERT_EQUAL_UINT8(0xF0, data.uvb_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.uvb_low);
    TEST_ASSERT_EQUAL_UINT8(0xF0, data.uvcomp1_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.uvcomp1_low);
    TEST_ASSERT_EQUAL_UINT8(0xF0, data.uvcomp2_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.uvcomp2_low);
    TEST_ASSERT_EQUAL(0, i2cWriteMockByte);
    TEST_ASSERT_EQUAL(true, i2cIsStoped);
    deinitApiObjects(&api_objects);
}
*/

void testSetResponseNotSuccessful()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    api_objects.bufferManager->buffer_can_fit = &mockBufferManagerBufferCanFit;
    api_objects.bufferManager->push = &mockBufferManagerPush;
    api_objects.bufferManager->destroy = &deinitBuffer;
    api_objects.bufferManager->create = &createBuffer;
    struct veml6075Data data = {0,0,0,0,0,0,0,0};
    setResponse(&data,api_objects.streamBuffers, api_objects.bufferManager, api_objects.response);

    TEST_ASSERT_EQUAL(ERROR_FOUND, api_objects.response->type);
    deinitApiObjects(&api_objects);
}

void testReadUVvalues()
{
    struct veml6075Data *data = {0,0,0,0,0,0,0,0};

    i2cWriteMockByte=0;

    readValues(UVA_DATA_REG, &(data->uva_high), &(data->uva_low));
    TEST_ASSERT_EQUAL_UINT8(0xF0, data->uvb_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data->uva_low);
    TEST_ASSERT_EQUAL(UVCOMP2_DATA_REG, i2cWriteMockByte);
    TEST_ASSERT_EQUAL(true, i2cIsStoped);
}

void testInitUvSensor()
{
    i2cWriteMockByte=1;

    initUVSensor();
    TEST_ASSERT_EQUAL(0, i2cWriteMockByte);
    TEST_ASSERT_EQUAL(true, i2cIsStoped);
}

void testEnterSleepMode()
{
    i2cWriteMockByte=1;

    enterSleepMode();
    TEST_ASSERT_EQUAL(0, i2cWriteMockByte);
    TEST_ASSERT_EQUAL(true, i2cIsStoped);
}

void testGetUvValues()
{
    struct veml6075Data data  ={0,0,0,0,0,0,0,0};
    i2cWriteMockByte=0;
    getUvValues(&data);
    TEST_ASSERT_EQUAL_UINT8(0xF0, data.uva_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.uva_low);
    TEST_ASSERT_EQUAL_UINT8(0xF0, data.uvb_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.uvb_low);
    TEST_ASSERT_EQUAL_UINT8(0xF0, data.uvcomp1_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.uvcomp1_low);
    TEST_ASSERT_EQUAL_UINT8(0xF0, data.uvcomp2_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.uvcomp2_low);
    TEST_ASSERT_EQUAL(0, i2cWriteMockByte);
    TEST_ASSERT_EQUAL(true, i2cIsStoped);
}

int main()
{
    UNITY_BEGIN();

    I2C.init();

    RUN_TEST(testSetResponseBufferTooSmall);
    RUN_TEST(testSetResponseBufferCannotPush);
    RUN_TEST(testSetResponseSuccessful);
    RUN_TEST(testSetResponseNotSuccessful);
    RUN_TEST(testReadUVvalues);
    RUN_TEST(testInitUvSensor);
    RUN_TEST(testEnterSleepMode);
    RUN_TEST(testGetUvValues);
    //RUN_TEST(testReadUvSensorHandle);


    return UNITY_END();
}
