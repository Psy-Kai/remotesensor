import qbs

CppApplication {
    name: "veml6075 Tests"

    files: [
        "veml6075test.c",
    ]

    Depends {
        name: "Unity"
    }

    Depends {
        name: "Sensors"
    }
}
