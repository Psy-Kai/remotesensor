#include <unity.h>
#include <apds_9250.h>
#include "apiobjects.h"
#include <unity.h>


char i2cReadWriteFlow;
unsigned char i2cWriteMockByte = 0;
bool i2cIsStoped;
unsigned char APDS_ADD = 0x0A;

uint8_t mockBufferManagerBufferCanFit(size_t size, Buffer *buffer)
{
    TEST_ASSERT_NOT_NULL(buffer);
    if ((buffer->count + size) <= buffer->size)
        return 1;
    return 0;
}

uint8_t mockBufferManagerBufferCanFitFailed(size_t size, Buffer *buffer)
{
    TEST_ASSERT_NOT_NULL(buffer);
    return 0;

    (void)size;
}

uint8_t mockBufferManagerPush(uint8_t value, Buffer *buffer)
{
    if (pow(256, sizeof(uint8_t)) <= buffer->count)
        return value + 1;
    if (buffer->size <= buffer->count)
        return value + 1;

    buffer->data[buffer->count] = value;
    ++(buffer->count);
    return value;
}

uint8_t mockBufferManagerPushFailed(uint8_t value, Buffer *buffer)
{
    return value + 1;
    (void)buffer;
}

void initBufferManager(struct BufferApi *buffer_manager)
{
    buffer_manager->buffer_can_fit = &mockBufferManagerBufferCanFit;
    buffer_manager->push = &mockBufferManagerPush;
}

void deinitBufferManager(struct BufferApi *buffer_manager)
{
    (void)buffer_manager;
}

void initBuffer(Buffer *buffer)
{
    buffer->count = 0;
    buffer->size = sizeof(uint16_t);
    buffer->data = buffer->head = malloc(buffer->size);
    buffer->end = buffer->tail = buffer->data + buffer->size;
}

Buffer* createBuffer(uint8_t num)
{
    Buffer* buffer = (Buffer*)malloc(sizeof(Buffer));
    buffer->count = 0;
    buffer->size = sizeof(uint8_t)*num;
    buffer->data = buffer->head = malloc(buffer->size);
    buffer->end = buffer->tail = buffer->data + buffer->size;
    return buffer;
}

void deinitBuffer(Buffer *buffer)
{
    free(buffer->data);
}

void initStreamBuffers(StreamBuffers *stream_buffers)
{
    stream_buffers->output = (Buffer*)malloc(sizeof(Buffer));
    initBuffer(stream_buffers->output);
}

void deinitStreamBuffers(StreamBuffers *stream_buffers)
{
    deinitBuffer(stream_buffers->output);
    free(stream_buffers->output);
}

void initApiObjects(struct ApiObjects *api_objects)
{
    apiObjectsInitEmpty(api_objects);
    api_objects->bufferManager = (struct BufferApi*)malloc(sizeof(struct BufferApi));
    initBufferManager(api_objects->bufferManager);
    api_objects->streamBuffers = (StreamBuffers*)malloc(sizeof(StreamBuffers));
    initStreamBuffers(api_objects->streamBuffers);
    api_objects->response = (Response*)malloc(sizeof(Response));
}

void deinitApiObjects(struct ApiObjects *api_objects)
{
    free(api_objects->response);
    deinitStreamBuffers(api_objects->streamBuffers);
    free(api_objects->streamBuffers);
    deinitBufferManager(api_objects->bufferManager);
    free(api_objects->bufferManager);
}

void i2cInitMock() { }

void i2cStopMock() { i2cIsStoped=true; }

unsigned char i2cStartMock(unsigned char address)
{
    i2cIsStoped=false;
    if(address>0) return 0;
    else return 1;
}

unsigned char i2cRepStartMock(unsigned char address)
{
    i2cIsStoped=false;
    if(address % 2 == 1)
    {
        i2cReadWriteFlow = 'R';
    }
    else i2cReadWriteFlow='W';
    if(address>0) return 0;
    else return 1;
}

void i2cStartWaitMock(unsigned char address)
{
    if(address>0) i2cIsStoped=false;
}

unsigned char i2cWriteMock(unsigned char data)
{
    i2cWriteMockByte = data;
    if(data!=0) return 0;
    return 1;
}

unsigned char i2cReadAckMock()
{
    if(i2cWriteMockByte==APDS_ADD) return 0x0f;
    return 1;
}

unsigned char i2cReadNakMock()
{
    if(i2cWriteMockByte==APDS_ADD) return 0xf0;
    return 1;
}

unsigned char i2cReadMock(unsigned char ack) { return 0xff; }

void initI2C(struct I2C *i2c)
{
    i2c->init = &i2cInitMock;
    i2c->readAck = &i2cReadAckMock;
    i2c->readNak = &i2cReadNakMock;
    i2c->repStart = &i2cRepStartMock;
    i2c->start = &i2cStartMock;
    i2c->startWait = &i2cStartWaitMock;
    i2c->stop = &i2cStopMock;
    i2c->write = &i2cWriteMock;
}


void testSetAPDS9250ResponseBufferTooSmall()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    api_objects.bufferManager->buffer_can_fit = &mockBufferManagerBufferCanFitFailed;
    api_objects.bufferManager->destroy = &deinitBuffer;
    api_objects.bufferManager->create = &createBuffer;
    struct apds9250Data data = {1,1,1,1,1,1,1,1,1,1,1,1};
    setAPDS9250Response(&data,api_objects.streamBuffers, api_objects.bufferManager, api_objects.response);
    TEST_ASSERT_EQUAL_UINT8(14, api_objects.streamBuffers->output->count);
    deinitApiObjects(&api_objects);
}

void testSetAPDS9250ResponseBufferCannotPush()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    api_objects.bufferManager->push = &mockBufferManagerPushFailed;
    api_objects.bufferManager->destroy = &deinitBuffer;
    api_objects.bufferManager->create = &createBuffer;
    struct apds9250Data data = {1,1,1,1,1,1,1,1,1,1,1,1};
    setAPDS9250Response(&data,api_objects.streamBuffers, api_objects.bufferManager, api_objects.response);
    TEST_ASSERT_EQUAL_UINT8(0, api_objects.streamBuffers->output->count);
    deinitApiObjects(&api_objects);
}

void testSetAPDS9250ResponseSuccessful()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    api_objects.bufferManager->buffer_can_fit = &mockBufferManagerBufferCanFit;
    api_objects.bufferManager->push = &mockBufferManagerPush;
    api_objects.bufferManager->destroy = &deinitBuffer;
    api_objects.bufferManager->create = &createBuffer;
    struct apds9250Data data = {0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0};
    setAPDS9250Response(&data,api_objects.streamBuffers, api_objects.bufferManager, api_objects.response);

    TEST_ASSERT_EQUAL(ACK, api_objects.response->type);
    TEST_ASSERT_EQUAL_UINT8(14, api_objects.streamBuffers->output->count);
    TEST_ASSERT_EQUAL_UINT8(sizeof(uint8_t), api_objects.response->word_size);
    TEST_ASSERT_EQUAL(UNSIGNED_TYPE, api_objects.response->content_type);
    deinitApiObjects(&api_objects);
}


void testSetAPDS9250ResponseNotSuccessful()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    api_objects.bufferManager->buffer_can_fit = &mockBufferManagerBufferCanFit;
    api_objects.bufferManager->push = &mockBufferManagerPush;
    api_objects.bufferManager->destroy = &deinitBuffer;
    api_objects.bufferManager->create = &createBuffer;
    struct apds9250Data data = {0,0,0,0,0,0,0,0,0,0,0,0};
    setAPDS9250Response(&data,api_objects.streamBuffers, api_objects.bufferManager, api_objects.response);

    TEST_ASSERT_EQUAL(ERROR_FOUND, api_objects.response->type);
    deinitApiObjects(&api_objects);
}

void testInitAPDS9250Sensor()
{
    struct I2C i2c;
    initI2C(&i2c);
    i2cWriteMockByte=0;

    initAPDS9250Sensor();
    TEST_ASSERT_EQUAL(1, i2cWriteMockByte);
    TEST_ASSERT_EQUAL(true, i2cIsStoped);
}


void testGetAPDS9250Values()
{
    struct apds9250Data data={0,0,0,0,0,0,0,0,0,0,0,0};
    struct I2C i2c;
    initI2C(&i2c);
    i2cWriteMockByte=0;
    getAPDS9250Values(&data);
    TEST_ASSERT_EQUAL_UINT8(0xF0, data.r_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.r_med);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.r_low);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.g_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.g_med);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.g_low);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.b_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.b_med);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.b_low);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.w_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.w_med);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.w_low);
    TEST_ASSERT_EQUAL(0x0A, i2cWriteMockByte);
    TEST_ASSERT_EQUAL(true, i2cIsStoped);
}
/* Commented out because of LOCAL_TESTS get undefined
void testReadAPDS9250SensorHandle()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    struct apds9250Data data={0,0,0,0,0,0,0,0,0,0,0,0};
    api_objects.bufferManager->buffer_can_fit = &mockBufferManagerBufferCanFit;
    api_objects.bufferManager->push = &mockBufferManagerPush;
    api_objects.bufferManager->destroy = &deinitBuffer;
    api_objects.bufferManager->create = &createBuffer;
    struct I2C i2c;
    initI2C(&i2c);
    i2cWriteMockByte=0;


    readAPDS9250SensorHandle(api_objects.streamBuffers, api_objects.request, api_objects.parameter,
                        api_objects.response, &i2c, api_objects.bufferManager, &data);
    TEST_ASSERT_EQUAL(ACK, api_objects.response->type);
    TEST_ASSERT_EQUAL_UINT8(14, api_objects.streamBuffers->output->count);
    TEST_ASSERT_EQUAL_UINT8(14, api_objects.streamBuffers->output->size);
    TEST_ASSERT_EQUAL_UINT8(sizeof(uint8_t), api_objects.response->word_size);
    TEST_ASSERT_EQUAL(UNSIGNED_TYPE, api_objects.response->content_type);
    TEST_ASSERT_EQUAL_UINT8(0xF0, data.r_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.r_med);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.r_low);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.g_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.g_med);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.g_low);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.b_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.b_med);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.b_low);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.w_high);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.w_med);
    TEST_ASSERT_EQUAL_UINT8(0x0F, data.w_low);
    TEST_ASSERT_EQUAL(0x0A, i2cWriteMockByte);
    TEST_ASSERT_EQUAL(true, i2cIsStoped);
    deinitApiObjects(&api_objects);
}
*/

int main()
{
    UNITY_BEGIN();

    RUN_TEST(testSetAPDS9250ResponseBufferTooSmall);
    RUN_TEST(testSetAPDS9250ResponseBufferCannotPush);
    RUN_TEST(testSetAPDS9250ResponseSuccessful);
    RUN_TEST(testSetAPDS9250ResponseNotSuccessful);
    RUN_TEST(testInitAPDS9250Sensor);
    RUN_TEST(testGetAPDS9250Values);
    //RUN_TEST(testReadAPDS9250SensorHandle);


    return UNITY_END();
}
