import qbs

CppApplication {
    name: "apds9250 Tests"

    files: [
        "apds9250test.c",
    ]

    Depends {
        name: "avr"
    }

    Depends {
        name: "Unity"
    }

    Depends {
        name: "Sensors"
    }
}
