import qbs

CppApplication {
    name: "BME680 Test"

    files: [
        "bme680test.c",
    ]

    cpp.defines: ["BME680_SELFTEST", "REMOTE_SENSOR_REGISTRATION"]

    Properties {
        condition: avr.flash && qbs.architecture.startsWith("avr")
        type: "flash_to_board"
    }

    Depends {
        name: "avr"
    }
    Depends {
        name: "Unity"
    }
    Depends {
        name: "Sensors"
    }

}
