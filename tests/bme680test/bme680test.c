#include <adapter_BME680.h>

#include <math.h>
#include <stdlib.h>
#include <unity.h>

#define ADCONVERTER_VALUE_8 0xF0
#define ADCONVERTER_VALUE_16 ((ADCONVERTER_VALUE_8 << 8) + ADCONVERTER_VALUE_8)

#ifdef REMOTE_SENSOR_REGISTRATION
uint8_t mockBufferManagerBufferCanFit(size_t size, Buffer *buffer)
{
    TEST_ASSERT_NOT_NULL(buffer);
    if ((buffer->count + size) <= buffer->size)
        return 1;
    return 0;
}

uint8_t mockBufferManagerBufferCanFitFailed(size_t size, Buffer *buffer)
{
    TEST_ASSERT_NOT_NULL(buffer);
    return 0;

    (void)size;
}

uint8_t mockBufferManagerPush(uint8_t value, Buffer *buffer)
{
    if (pow(256, sizeof(uint8_t)) <= buffer->count)
        return value + 1;
    if (buffer->size <= buffer->count)
        return value + 1;

    buffer->data[buffer->count] = value;
    ++(buffer->count);
    return value;
}

uint8_t mockBufferManagerPushFailed(uint8_t value, Buffer *buffer)
{
    return value + 1;
    (void)buffer;
}

void initBufferManager(struct BufferApi *buffer_manager)
{
    buffer_manager->buffer_can_fit = &mockBufferManagerBufferCanFit;
    buffer_manager->push = &mockBufferManagerPush;
}

void deinitBufferManager(struct BufferApi *buffer_manager)
{
    (void)buffer_manager;
}

void initBuffer(Buffer *buffer)
{
    buffer->count = 0;
    buffer->size = 16;
    buffer->data = buffer->head = malloc(buffer->size);
    buffer->end = buffer->tail = buffer->data + buffer->size;
}

void deinitBuffer(Buffer *buffer)
{
    free(buffer->data);
}

void initStreamBuffers(StreamBuffers *stream_buffers)
{
    stream_buffers->output = (Buffer*)malloc(sizeof(Buffer));
    initBuffer(stream_buffers->output);
}

void deinitStreamBuffers(StreamBuffers *stream_buffers)
{
    deinitBuffer(stream_buffers->output);
    free(stream_buffers->output);
}

void initApiObjects(struct ApiObjects *api_objects)
{
    apiObjectsInitEmpty(api_objects);
    api_objects->bufferManager = (struct BufferApi*)malloc(sizeof(struct BufferApi));
    initBufferManager(api_objects->bufferManager);
    api_objects->streamBuffers = (StreamBuffers*)malloc(sizeof(StreamBuffers));
    initStreamBuffers(api_objects->streamBuffers);
    api_objects->response = (Response*)malloc(sizeof(Response));
}

void deinitApiObjects(struct ApiObjects *api_objects)
{
    free(api_objects->response);
    deinitStreamBuffers(api_objects->streamBuffers);
    free(api_objects->streamBuffers);
    deinitBufferManager(api_objects->bufferManager);
    free(api_objects->bufferManager);
}

void testReadBME680Successful()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    //create_and_initalize_BME680_device(BME680_I2C_ADDR_SECONDARY,BME680_GAS_SENSOR_ENABLED);
    const struct bme680_dev *dev = return_device_ptr_BME680();
    const struct bme680_field_data *data = return_data_field_ptr_BME680();

    TEST_ASSERT_TRUE(get_sensor_data_from_buffer_BME680(&api_objects));
    TEST_ASSERT_EQUAL_UINT8(18, api_objects.streamBuffers->output->count);
    const uint8_t responseData[] = { 'B', '0', ADCONVERTER_VALUE_8, ADCONVERTER_VALUE_8 };
    TEST_ASSERT_EQUAL_INT8_ARRAY(responseData, api_objects.streamBuffers->output->data,
                                 api_objects.streamBuffers->output->size);
    TEST_ASSERT_EQUAL_UINT8(sizeof(uint8_t), api_objects.response->word_size);
    TEST_ASSERT_EQUAL(UNSIGNED_TYPE, api_objects.response->content_type);

    deinitApiObjects(&api_objects);
}

void testReadBME680BufferTooSmall()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    api_objects.bufferManager->buffer_can_fit = &mockBufferManagerBufferCanFitFailed;
    //create_and_initalize_BME680_device(BME680_I2C_ADDR_SECONDARY,BME680_GAS_SENSOR_ENABLED);
    const struct bme680_dev *dev = return_device_ptr_BME680();
    const struct bme680_field_data *data = return_data_field_ptr_BME680();

    TEST_ASSERT_FALSE(get_sensor_data_from_buffer_BME680(&api_objects));
    TEST_ASSERT_EQUAL_UINT8(0, api_objects.streamBuffers->output->count);
}

void testReadBME680BufferCannotPush()
{
    ApiObjects api_objects;
    initApiObjects(&api_objects);
    api_objects.bufferManager->push = &mockBufferManagerPushFailed;
    //create_and_initalize_BME680_device(BME680_I2C_ADDR_SECONDARY,BME680_GAS_SENSOR_ENABLED);
    const struct bme680_dev *dev = return_device_ptr_BME680();
    const struct bme680_field_data *data = return_data_field_ptr_BME680();

    TEST_ASSERT_FALSE(get_sensor_data_from_buffer_BME680(&api_objects));
    TEST_ASSERT_EQUAL_UINT8(0, api_objects.streamBuffers->output->count);
}
#endif

int main(void)
{
    UNITY_BEGIN();

#ifdef REMOTE_SENSOR_REGISTRATION
    RUN_TEST(testReadBME680Successful);
    RUN_TEST(testReadBME680BufferTooSmall);
    RUN_TEST(testReadBME680BufferCannotPush);
#endif

    return UNITY_END();
}
