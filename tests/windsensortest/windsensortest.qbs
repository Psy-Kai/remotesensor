import qbs

CppApplication {
    name: "Windsensor Test"

    files: [
        "windsensortest.c",
    ]

    Depends {
        name: "avr"
    }
    Depends {
        name: "Unity"
    }
    Depends {
        name: "Sensors"
    }
}
