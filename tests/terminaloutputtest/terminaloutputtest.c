#include <unity.h>
#include <logger.h>
#include <terminaloutput.h>

#define TEST_MESSAGE "Hello World";

char kBuffer[TERMINAL_OUTPUT_BUFFER_SIZE];
void resetBuffer()
{
    for (uint8_t i = 0; i < TERMINAL_OUTPUT_BUFFER_SIZE; ++i)
        kBuffer[i] = 0;
}

void clearTerminalOutputBuffer()
{
    for (uint8_t i = 0; i < TERMINAL_OUTPUT_BUFFER_SIZE; ++i)
        TerminalOutput.buffer[i] = 0;
    TerminalOutput.buffer_pointer = TerminalOutput.buffer;
}

void testTerminalOutputInit()
{
    TEST_ASSERT_TRUE(TerminalOutput.init());

    TEST_ASSERT_EACH_EQUAL_UINT8(0, TerminalOutput.buffer, TERMINAL_OUTPUT_BUFFER_SIZE);
    TEST_ASSERT_EQUAL(TerminalOutput.buffer, TerminalOutput.buffer_pointer);
}

void testTerminalOutputPutChar()
{
    char c[] = TEST_MESSAGE;
    for (uint8_t i = 0; c[i]; ++i)
        TEST_ASSERT_TRUE(TerminalOutput.putChar(c[i]));

    TEST_ASSERT_EQUAL_UINT8_ARRAY(c, TerminalOutput.buffer, sizeof(c));
    TEST_ASSERT_EQUAL_UINT8(0, TerminalOutput.buffer[sizeof(c)]);

    clearTerminalOutputBuffer();
}

void testTerminalOutputFlush()
{
    char message[] = TEST_MESSAGE;
    for (uint8_t i = 0; message[i]; ++i)
        TerminalOutput.putChar(message[i]);

    TerminalOutput.flush();

    TEST_ASSERT_EACH_EQUAL_UINT8(0, TerminalOutput.buffer, TERMINAL_OUTPUT_BUFFER_SIZE);
    TEST_ASSERT_EQUAL(TerminalOutput.buffer, TerminalOutput.buffer_pointer);
}

int main()
{
    UNITY_BEGIN();

    RUN_TEST(testTerminalOutputInit);
    RUN_TEST(testTerminalOutputPutChar);
    RUN_TEST(testTerminalOutputFlush);

    return UNITY_END();
}
