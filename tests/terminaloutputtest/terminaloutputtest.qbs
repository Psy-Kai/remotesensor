import qbs

CppApplication {
    name: "Terminal Output Test"

    files: [
        "terminaloutputtest.c",
    ]

    Depends {
        name: "avr"
    }
    Depends {
        id: remoteSensorDepends
        name: "Remote Sensor"
    }
    Depends {
        name: "Sensors"
    }
    Depends {
        name: "Unity"
    }
}
