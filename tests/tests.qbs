import qbs

Project {
    name: "Tests"

    references: [
        "adconvertertest",
        "addressconverttest",
        "firsttest",
        "outputadcvalues",
        "terminaloutputtest",
        "terminaloutputunitytest",
        "windsensortest",
        "BME680test",
        "apds9250test",
        "veml6075_test",
        "sensors_field_test"
    ]
}
