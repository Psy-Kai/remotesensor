import qbs

CppApplication {
    name: "Terminal Output Unity Test"

    files: [
        "terminaloutputunitytest.c",
    ]

    Depends {
        name: "avr"
    }
    Depends {
        name: "Sensors"
    }
    Depends {
        name: "Unity"
    }
}
