#include <unity.h>
#include <terminaloutput.h>

struct TerminalOutput kOriginalTerminalOutput;
uint8_t kTerminalOutputInitCalls = 0;
uint8_t kTerminalOutputPutCharCalls = 0;
uint8_t kTerminalOutputFlushCalls = 0;

bool mockTerminalOutputInit()
{
    ++kTerminalOutputInitCalls;
    return true;
}

bool mockTerminalOutputPutChar(char ch)
{
    ++kTerminalOutputPutCharCalls;
    return true;

    (void)ch;
}

void mockTerminalOutputFlush()
{
    ++kTerminalOutputFlushCalls;
}

void setupMock()
{
    kTerminalOutputInitCalls = 0;
    kTerminalOutputPutCharCalls = 0;
    kTerminalOutputFlushCalls = 0;
    TerminalOutput.init = &mockTerminalOutputInit;
    TerminalOutput.putChar = &mockTerminalOutputPutChar;
    TerminalOutput.flush = &mockTerminalOutputFlush;
}

void shutdownMock()
{
    TerminalOutput = kOriginalTerminalOutput;
}

void checkTerminalOutputInitCalls()
{
    TEST_ASSERT_EQUAL_UINT8(1, kTerminalOutputInitCalls);
}

void testUnityPrint()
{
    setupMock();

    char message[] = "Hello World";
    UnityPrint(message);

    shutdownMock();

    TEST_ASSERT_EQUAL_UINT8(sizeof(message)-1, kTerminalOutputPutCharCalls);
}

int main()
{
    TerminalOutput.init();
    kOriginalTerminalOutput = TerminalOutput;
    setupMock();

    UNITY_BEGIN();

    shutdownMock();
    TerminalOutput.init();
    RUN_TEST(checkTerminalOutputInitCalls);
    RUN_TEST(testUnityPrint);

    return UNITY_END();
}

