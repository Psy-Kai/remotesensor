import qbs

CppApplication {
    name: "Output ADC values"

    files: [
        "outputadcvalues.c",
    ]

    Depends {
        name: "avr"
    }
    Depends {
        name: "Unity"
    }
    Depends {
        name: "Sensors"
    }
}
