#include <inttypes.h>
#include <terminaloutputunitydefines.h>
#include <unity.h>
#include <adconverter.h>

void mockAdConverterInit()
{}

ADConverterValue mockAdConverterRead(uint8_t pin)
{
    ADConverterValue result = { 0x3FF };
    return result;
    (void)pin;
}

void outputMessage(const char *message)
{
    UnityPrint(message);
    UNITY_OUTPUT_FLUSH();
}

int main()
{
#if !defined(__AVR__)
    ADConverter.init = &mockAdConverterInit;
    ADConverter.read = &mockAdConverterRead;
#endif

    UNITY_OUTPUT_START();

    outputMessage("init ADConverter");
    ADConverter.init();
    outputMessage("start reading");
    char message[25];
    while (1) {
        ADConverterValue value = ADConverter.read(1);
        sprintf(message, "Value: %"PRIi16, value.value);
        outputMessage(message);
    }

    return 1;
}
