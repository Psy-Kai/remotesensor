import qbs

Project {
    name: "Sensors"

    references: [
        "doc",
        "lib/lib.qbs",
        "src",
        "tests",
    ]

    AutotestRunner {}
    qbsSearchPaths: [
        "lib",
        "qbs-additionals",
    ]
}
