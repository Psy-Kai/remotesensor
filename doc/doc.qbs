import qbs
import qbs.FileInfo
import qbs.Process
import qbs.TextFile

Product {
    name: "Documentation"
    type: "doxygen"

    property string doxygen: "doxygen"
    property bool internal: false

    files: [
        "doxygen/Doxyfile",
        "doxygen/build.dox",
        "doxygen/mainpage.dox",
    ]

    FileTagger {
        fileTags: "doxyfile"
        patterns: "Doxyfile"
    }

    Rule {
        inputs: "doxyfile"
        outputFileTags: [
            "doxygen"
        ]
        outputArtifacts: {
            return [{ fileTags: "doxygen", filePath: "html" }];
        }
        prepare: {
            var cmd = new JavaScriptCommand();
            cmd.description = "Generate Documentation with " + product.doxygen;
            cmd.highlight = "filegen";
            cmd.sourceCode = function() {
                var doxyfile = new TextFile(input.filePath);
                var fileContent = doxyfile.readAll();
                if (fileContent === "")
                    throw "empty Doxygen file";

                var process = new Process();
                process.setWorkingDirectory(product.sourceDirectory + "/../");
                if (!process.start(product.doxygen, ["-"]))
                    throw "Could not run " + product.doxygen;
                process.write(fileContent);
                process.writeLine("OUTPUT_DIRECTORY=\"" + product.buildDirectory + "\"");
                process.writeLine("INTERNAL_DOCS=" + (product.internal ? "YES" : "NO"))
                process.closeWriteChannel();
                while (!process.waitForFinished(100)) {
                    var output = process.readStdOut();
                    if (output !== "")
                        console.info(output);
                }
                if (process.exitCode())
                    throw process.readStdErr() + "\n" + process.readStdOut();
                else
                    console.info(process.readStdOut());
            };
            return [cmd]
        }
    }
}
