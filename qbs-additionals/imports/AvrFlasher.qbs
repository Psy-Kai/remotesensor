import qbs

Product {
    property string dfuProgrammer: "dfu-programmer"
    property string target: "atmega32u4"

    builtByDefault: false

    type: [
        "flash_to_board"
    ]

    Rule {
        inputsFromDependencies: "hex"
        outputFileTags: "flash_to_board"
        Artifact {
            filePath: ""
            fileTags: "flash_to_board"
        }
        prepare: {
            var erase = new Command();
            erase.program = product.dfuProgrammer;
            erase.arguments = [
                        product.target,
                        "erase"
                    ];
            erase.description = "erase controller flash"

            var flash = new Command();
            flash.program = product.dfuProgrammer;
            flash.arguments = [
                        product.target,
                        "flash",
                        input.filePath
                    ];
            flash.description = "flash application";

            var launch = new Command();
            launch.program = product.dfuProgrammer;
            launch.arguments = [
                        product.target,
                        "launch"
                    ];
            launch.description = "launch application on controller";

            return [erase, flash];
        }
    }
}
