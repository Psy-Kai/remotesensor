import qbs
import qbs.FileInfo
import qbs.TextFile

Module {
    property path path
    property string mcu: "atmega32u4"
    property string arch: "AVR8"
    property string board: "USBKEY"
    property int f_cpu: 8000000
    property int f_usb: f_cpu
    property string optimization: "s"
    property stringList dmbsLufaModules: [
        "lufa-sources.mk",
        "lufa-gcc.mk",
    ]
    property stringList dmbsCommonModules: [
        "core.mk",
        "cppcheck.mk",
        "doxygen.mk",
        "dfu.mk",
        "gcc.mk",
        "hid.mk",
        "avrdude.mk",
        "atprogram.mk",
    ]

    FileTagger {
        fileTags: "cxx"
        patterns: "*.c"
    }

    FileTagger {
        fileTags: "lufa_config"
        patterns: "LUFAConfig.h"
    }

    Rule {
        inputs: [
            "cxx",
            "lufa_config",
        ]
        outputFileTags: "lufa_makefile"
        multiplex: true
        outputArtifacts: [
            {
                filePath: "makefile",
                fileTags: "lufa_makefile"
            },
            {
                filePath: "obj",
                fileTags: "dummyTags"
            }
        ]
        prepare: {
            var cmd = new JavaScriptCommand();
            cmd.description = "fill makefile";
            cmd.highlight = "filegen";
            cmd.sourceCode = function() {
                console.error("Path to LUFA: " + product.lufa.path);
                if (product.lufa.path.match(" ")) {
                    console.error("Path to LUFA: " + product.lufa.path);
                    throw "Path to LUFA cannot contain whitespaces";
                }

                var lufaConfig = inputs.lufa_config[0] === undefined ? null
                                                                     : inputs.lufa_config[0];
                if (inputs.lufa_config[1] !== undefined)
                    throw "too many LUFAConfig.h";

                var inputSourceFiles = [];
                for (i in inputs.cxx) {
                    var filePath = inputs.cxx[i].filePath;
                    if (filePath.match(" ")) {
                        console.error("Filepath: " + filePath);
                        throw "Filepath cannot contain whitespaces";
                    }
                    inputSourceFiles = inputSourceFiles.concat(filePath);
                }

                var makefile = new TextFile(outputs.lufa_makefile[0].filePath, TextFile.WriteOnly);

                makefile.writeLine("MCU = " + product.lufa.mcu);
                makefile.writeLine("ARCH = " + product.lufa.arch);
                makefile.writeLine("BOARD = " + product.lufa.board);
                makefile.writeLine("F_CPU = " + product.lufa.f_cpu);
                makefile.writeLine("F_USB = " + product.lufa.f_usb);
                makefile.writeLine("OPTIMIZATION = " + product.lufa.optimization);
                makefile.writeLine("TARGET = " + product.targetName);
                makefile.writeLine("SRC = " + inputSourceFiles.join(" ") + " $(LUFA_SRC_USB) $(LUFA_SRC_USBCLASS)");
                makefile.writeLine("LUFA_PATH = " + product.lufa.path + "/LUFA");
                if (lufaConfig === null) {
                    makefile.writeLine("CC_FLAGS = ");
                } else {
                    var pathToLufaConfig = FileInfo.path(lufaConfig.filePath);
                    makefile.writeLine("CC_FLAGS = -DUSE_LUFA_CONFIG_HEADER -I" + pathToLufaConfig);
                }
                makefile.writeLine("LD_FLAGS = ");

                makefile.writeLine("");

                makefile.writeLine("all:")

                makefile.writeLine("");

                makefile.writeLine("DMBS_LUFA_PATH ?= $(LUFA_PATH)/Build/LUFA");
                product.lufa.dmbsLufaModules.forEach(function (module) {
                    makefile.writeLine("include $(DMBS_LUFA_PATH)/" + module);
                });

                makefile.writeLine("");

                makefile.writeLine("DMBS_PATH ?= $(LUFA_PATH)/Build/DMBS/DMBS");
                product.lufa.dmbsCommonModules.forEach(function (module) {
                    makefile.writeLine("include $(DMBS_PATH)/" + module);
                });
            }
            return [cmd];
        }
    }

    Rule {
        inputs: "lufa_makefile"
        outputFileTags: "staticlibrary"
        Artifact {
            filePath: "lib" + product.targetName + ".a"
            fileTags: "staticlibrary"
        }
        prepare: {
            var cmd = new Command();
            cmd.program = "make";
            cmd.arguments = ["-C", FileInfo.path(input.filePath), "lib"];
            cmd.highlight = "compiler";
            cmd.description = "create lufa library " + product.name;
            return [cmd];
        }
    }
}
