import qbs
import qbs.FileInfo

/*
avr-objcopy -j.text -j .data -O ihex <input> <output>
*/

Module {
    property string avrobjcopy: FileInfo.joinPaths(product.cpp.toolchainInstallPath, "avr-objcopy")

    additionalProductTypes: [ "hex" ]

    Rule {
        condition: qbs.architecture.startsWith("avr")
        inputs: "application"
        outputFileTags: "hex"
        Artifact {
            filePath: input.baseName + '.hex'
            fileTags: "hex"
        }
        prepare: {
            var cmd = new Command();
            cmd.program = product.avr.avrobjcopy;
            cmd.arguments = [
                        "-j", ".text",
                        "-j", ".data",
                        "-O", "ihex",
                        input.filePath, output.filePath
                    ];
            cmd.description = "create hex file";
            return [cmd];
        }
    }
}
