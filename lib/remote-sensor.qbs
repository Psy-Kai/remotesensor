import qbs

StaticLibrary {
    name: "Remote Sensor"

    property string projectDirPath
    property int cpuFreq: 8000000
    condition: {
        if ((projectDirPath !== undefined) && (projectDirPath !== ""))
            return true;

        throw "Remote Sensor API has a strong dependency! Cannot build without!!!"
        return false;
    }

    files: [
        projectDirPath + '/lib/remote-sensor/**/*.c',
        projectDirPath + '/lib/remote-sensor/**/*.h',
    ]

    cpp.defines: {
        var defines = [
                    "F_CPU=" + cpuFreq + "UL"
                ]
        //defines = defines.concat("LOCAL_TESTS");
        if (IEEE802154.present)
            defines = defines.concat("IEEE_802154");

        return defines;
    }

    cpp.includePaths: [
        projectDirPath + '/lib',
        projectDirPath + '/tests/communication',
        projectDirPath + '/app/src/communications',
    ].concat(sourceDirectory + '/' + communicationMiddleware.prefix)

    Group {
        id: communicationMiddleware
        name: "Communication Middleware"
        prefix: "remote-sensor/"
        files: [
            "802154adapter.c",
            "802154adapter.h",
            "CommunicationMiddleware.h",
        ]
    }

    Group {
        id: usartCommunication
        name: "USART Communication"

        condition: qbs.architecture.startsWith("avr")
        prefix: projectDirPath + '/app/src/communications/'
        files: [
            "logger.h",
            "usart_controller.c",
            "usart_controller.h",
            "usart_logger.c",
        ]
    }

    Properties {
        condition: !qbs.architecture.startsWith("avr")
        cpp.defines: [
            "LOCAL_TESTS"
        ].concat(outer)
        cpp.includePaths: [
            projectDirPath + '/tests',
        ].concat(outer)

        files: [
            projectDirPath + "/tests/communication/dummy/watchdog.c",
            projectDirPath + "/tests/communication/dummy/watchdog.h",
        ].concat(outer)
    }

    Depends {
        name: "cpp"
    }
    Depends {
        name: "CException"
        condition: IEEE802154.condition
        required: false
    }
    Depends {
        id: IEEE802154
        name: "802154"
        required: false
    }

    Export {
        Depends {
            name: "cpp"
        }
        Depends {
            name: "CException"
            condition: IEEE802154.condition
            required: false
        }

        Properties {
            condition: qbs.architecture.startsWith("avr")
            cpp.staticLibraries: CException.libraryPath
        }
        cpp.includePaths: product.cpp.includePaths
        cpp.defines: product.cpp.defines
        readonly property bool usartEnabled: usartCommunication.condition
    }
}
