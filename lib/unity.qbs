import qbs

StaticLibrary {
    name: "Unity"

    property string projectDirPath
    condition: (projectDirPath !== undefined) && (projectDirPath !== "")

    files: [
        "dummy.c",
        projectDirPath + '/src/unity.c',
        projectDirPath + '/src/unity.h',
        projectDirPath + '/src/unity_internals.h',
    ]

    cpp.includePaths: projectDirPath + '/src'

    Depends {
        name: "cpp"
    }
    Depends {
        name: "Terminal Output"
    }

    Export {
        cpp.includePaths: product.projectDirPath + '/src'
        Depends {
            name: "cpp"
        }
        Depends {
            name: "Terminal Output"
        }
    }
}
