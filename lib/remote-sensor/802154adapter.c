#include "802154adapter.h"
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <stdlib.h>

#if defined(IEEE_802154) && defined(__AVR__)
#define USE_IEEE_802154
#
#endif // IEEE_802154

#if defined(__AVR__)
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#endif // __AVR__


#ifdef USE_IEEE_802154
#include <Mac802154.h>
#include <Mac802154MRFImpl.h>
#include <Peripheral/PeripheralSPIImpl.h>
#endif // USE_IEEE_802154

/* Prototype*/
void mrfAdapterInit(void);

/* Adapter function for remote-sensor api controller.c */
void CommunicationMiddlewareAPI_init(void) {
  mrfAdapterInit();
}

void debug(const char* string) {}


#if defined(__AVR__)
void delay_microseconds(double microseconds) {
  while (microseconds > 0){
    _delay_ms(1);
    microseconds--;
  }
}
#endif // __AVR__


uint16_t addressArrayToInt16(uint8_t *address)
{
    uint16_t result = 0;

    for (size_t i = 0; i < sizeof(result); ++i)
        result += (uint16_t)address[i] << ((sizeof(result)-(i+1))*8);

    return result;
}

uint64_t addressArrayToInt64(uint8_t *address)
{
    uint64_t result = 0;

    for (size_t i = 0; i < sizeof(result); ++i)
      result += (uint64_t)address[i] << ((sizeof(result)-(i+1))*8);

    return result;
}

#ifdef USE_IEEE_802154

static Mac802154 *ieee_802154 = NULL;
static PeripheralInterface peripheral_interface = NULL;
static uint8_t *packet = NULL;
static uint8_t payload_size = 0;

static SPIConfig spi_config = {
    .data_register = &SPDR,
    .clock_pin = PORTB1,
    .miso_pin = PORTB3,
    .mosi_pin = PORTB2,
    .slave_select_pin = PORTB0,
    .io_lines_data_direction_register = &DDRB,
    .io_lines_data_register = &PORTB,
    .status_register = &SPSR,
    .control_register = &SPCR,
};

PeripheralSPI mrf_spi_client = {
    .data_register = &PORTB,
    .data_direction_register = &DDRB,
    .select_chip_pin_number = 4,
    .data_order = SPI_DATA_ORDER_MSB_FIRST,
    .spi_mode = SPI_MODE_0,
    .idle_signal = SPI_IDLE_SIGNAL_HIGH,
    .clock_rate_divider = SPI_CLOCK_RATE_DIVIDER_64,
};

static Mac802154Config config = {
        .channel = 0x0C,
        .pan_id = 0x2015,
        .short_source_address = 0xAACC,
        .extended_source_address = 0x0013A20040C04EE3,
        .device = &mrf_spi_client,
};

void mrfAdapterInit(void)
{
    peripheral_interface = malloc(PeripheralInterfaceSPI_requiredSize());
    PeripheralInterfaceSPI_createNew((uint8_t*)peripheral_interface, &spi_config);
    PeripheralInterface_init(peripheral_interface);
    PeripheralInterface_configurePeripheral(peripheral_interface, &mrf_spi_client);
    config.interface = peripheral_interface;
    ieee_802154 = malloc(Mac802154MRF_requiredSize());
    uint8_t * rawMemory = (uint8_t*)ieee_802154;
    Mac802154MRF_create(rawMemory, delay_microseconds);
    Mac802154_init(ieee_802154, &config);

}

/* unused */
int8_t mrfAdapterReceiveFrame()
{
    abort();
    return 0;
}

int8_t mrfAdapterSendFrameTo16BitAddress(const uint8_t *address, const uint8_t *payload,
                                           uint8_t payload_size, uint8_t option)
{
    Mac802154_setShortDestinationAddress(ieee_802154, address);
    Mac802154_setPayload(ieee_802154, payload, payload_size);
    Mac802154_sendBlocking(ieee_802154);
    (void)option;
    return 0;
}

int8_t mrfAdapterSendFrameTo64BitAddress(const uint8_t *address, const uint8_t *payload,
                                           uint8_t payload_size, uint8_t option)
{
    Mac802154_setExtendedDestinationAddress(ieee_802154,addressArrayToInt64(address));
    Mac802154_setPayload(ieee_802154, payload, payload_size);
    Mac802154_sendBlocking(ieee_802154);
    (void)option;
//Set LED PORTC7 high and shut off after 50ms
#if defined(__AVR__)
     PORTC |= _BV(PORTC7);
     _delay_ms(50);
     PORTC &= ~(_BV(PORTC7));
#endif // __AVR__
    return 0;
}

bool mrfAdapterReceivedNewFrame(void)
{
  bool result = Mac802154_newPacketAvailable(ieee_802154);
  if (result && (packet != NULL)){
      free(packet);
      packet = NULL;
      payload_size = 0;
  }
  return result;
}

uint8_t mrfAdapterGetReceivedPayloadSize(void)
{
  if (packet == NULL) {
      uint8_t size = Mac802154_getReceivedPacketSize(ieee_802154);
      packet = (uint8_t*)malloc(size);
      Mac802154_fetchPacketBlocking(ieee_802154, packet, size);
      payload_size = Mac802154_getPacketPayloadSize(ieee_802154, packet);
  }
  return payload_size;
}

void mrfAdapterGetReceivedPayload(uint8_t *payload_destination)
{
  if (packet != NULL && payload_size > 0){
    memmove(payload_destination,Mac802154_getPacketPayload(ieee_802154, packet),payload_size);
  }
}

int8_t mrfAdapterGet16BitSourceAddressOfReceivedMessage(uint8_t *array_to_hold_16bit_address)
{
  abort();
  (void)array_to_hold_16bit_address;
}

int8_t mrfAdapterGet64BitSourceAddressOfReceivedMessage(uint8_t *array_to_hold_64bit_address)
{
    abort();
    (void)array_to_hold_64bit_address;
}

void mrfAdapterSet16BitDestinationAddressFromArray(const uint8_t *packet)
{
  Mac802154_setShortDestinationAddressFromArray(ieee_802154, Mac802154_getPacketSourceAddress(ieee_802154, packet));
}

void mrfAdapterInterruptHandler(void)
{
//    peripheral_interface->handleInterrupt();
}
#else
void mrfAdapterInit()
{
    abort();
}

int8_t mrfAdapterReceiveFrame()
{
    abort();
}

int8_t mrfAdapterSendFrameTo16BitAddress(const uint8_t *address, const uint8_t *payload,
                                           uint8_t payload_size, uint8_t option)
{
    (void)address;
    (void)payload;
    (void)payload_size;
    (void)option;
    abort();
}

int8_t mrfAdapterSendFrameTo64BitAddress(const uint8_t *address, const uint8_t *payload,
                                           uint8_t payload_size, uint8_t option)
{
    (void)address;
    (void)payload;
    (void)payload_size;
    (void)option;
    abort();
}

bool mrfAdapterReceivedNewFrame(void)
{
    abort();
}

uint8_t mrfAdapterGetReceivedPayloadSize(void)
{
    abort();
}

void mrfAdapterGetReceivedPayload(uint8_t *payload_destination)
{
    (void)payload_destination;
    abort();
}

int8_t mrfAdapterGet16BitSourceAddressOfReceivedMessage(uint8_t *array_to_hold_16bit_address)
{
    (void)array_to_hold_16bit_address;
    abort();
}

int8_t mrfAdapterGet64BitSourceAddressOfReceivedMessage(uint8_t *array_to_hold_64bit_address)
{
    (void)array_to_hold_64bit_address;
    abort();
}

void mrfAdapterInterruptHandler(void)
{
    abort();
}
#endif // USE_IEEE_802154

struct CommunicationMiddlewareAPI CommunicationMiddlewareAPI = {
    .init = mrfAdapterInit,
    .receiveFrame = mrfAdapterReceiveFrame,
    .sendFrameTo16BitAddress = mrfAdapterSendFrameTo16BitAddress,
    .sendFrameTo64BitAddress = mrfAdapterSendFrameTo64BitAddress,
    .receivedNewFrame = mrfAdapterReceivedNewFrame,
    .getReceivedPayloadSize = mrfAdapterGetReceivedPayloadSize,
    .getReceivedPayload = mrfAdapterGetReceivedPayload,
    .get16BitSourceAddressOfReceivedMessage = mrfAdapterGet16BitSourceAddressOfReceivedMessage,
    .get64BitSourceAddressOfReceivedMessage = mrfAdapterGet64BitSourceAddressOfReceivedMessage,
    .interruptHandler = mrfAdapterInterruptHandler
};
