#pragma once

#include <stdint.h>
#include <stdbool.h>

uint16_t addressArrayToInt16(uint8_t *address);
uint64_t addressArrayToInt64(uint8_t *address);
void mrfAdapterSet16BitDestinationAddressFromArray(const uint8_t *packet);
void mrfAdapterFetchReceivedPacket(uint8_t *payload_destination);
void CommunicationMiddlewareAPI_init(void);

struct CommunicationMiddlewareAPI
{
    void (*init) (void);

    int8_t (*receiveFrame) (void);

    int8_t (*sendFrameTo16BitAddress) (const uint8_t *address,
            const uint8_t *payload, uint8_t payload_size, uint8_t option);
    int8_t (*sendFrameTo64BitAddress) (const uint8_t *address,
            const uint8_t *payload, uint8_t payload_size, uint8_t option);

    bool (*receivedNewFrame) (void);

    uint8_t (*getReceivedPayloadSize) (void);

    void (*getReceivedPayload) (uint8_t *payload_destination);

    int8_t (*get16BitSourceAddressOfReceivedMessage) (uint8_t *array_to_hold_16bit_address);
    int8_t (*get64BitSourceAddressOfReceivedMessage) (uint8_t *array_to_hold_64bit_address);

    void (*interruptHandler) (void);
};

extern struct CommunicationMiddlewareAPI CommunicationMiddlewareAPI;
