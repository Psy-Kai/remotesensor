import qbs

StaticLibrary {
    id: root
    name: "I2C Master"

    property string projectDirPath
    condition: (projectDirPath !== undefined) &&
               (projectDirPath !== "")

    files: [
        projectDirPath + "/twimaster.c",
        projectDirPath + "/i2cmaster.h",
    ]

    cpp.includePaths: [
        projectDirPath
    ]

    Depends {
        name: "cpp"
    }

    Export {
        condition: product.condition
        Depends {
            name: "cpp"
        }
        cpp.includePaths: product.projectDirPath
        cpp.defines: "I2CMASTER"
    }
}
