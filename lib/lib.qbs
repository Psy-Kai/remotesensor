import qbs

Project {
    name: "External Libraries"

    references: [
        "cexception.qbs",
        "i2cmaster.qbs",
        "remote-sensor.qbs",
        "lufa/lufa.qbs",
        "unity.qbs",
        "BME680Driver.qbs",
    ]
}
