import qbs

StaticLibrary {
    name: "BME680Driver"

    property string projectDirPath
    condition: (projectDirPath !== undefined) && (projectDirPath !== "")

    files: [
        projectDirPath + '/bme680.c',
        projectDirPath + '/bme680.h',
        projectDirPath + '/bme680_defs.h',
        projectDirPath + '/Self test/bme680_selftest.h',
        projectDirPath + '/Self test/bme680_selftest.c',
    ]

    cpp.includePaths: projectDirPath
    cpp.defines: "BME680_FLOAT_POINT_COMPENSATION"

    Depends {
        name: "cpp"
    }

    Export {
        condition: product.condition
        Depends {
            name: "cpp"
        }
        cpp.includePaths: product.cpp.includePaths
    }
}
