import qbs
import qbs.FileInfo

StaticLibrary {
    name: "CException"

    property string projectDirPath
    condition: (projectDirPath !== undefined) && (projectDirPath !== "")

    files: [
        projectDirPath + '/lib/CException.c',
        projectDirPath + '/lib/CException.h',
    ]

    Depends {
        name: "cpp"
    }

    Export {
        property path libraryPath: FileInfo.joinPaths(product.buildDirectory,
                                                       product.targetName)

        cpp.includePaths: product.projectDirPath + '/lib'
        Depends {
            name: "cpp"
        }
    }
}
