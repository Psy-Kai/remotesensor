import qbs

Project {
    name: "LUFA"

    Product {
        name: "LUFA"

        property string projectDirPath
        condition: projectDirPath
        lufa.path: projectDirPath

        Depends {
            name: "lufa"
        }
        Export {
            Depends {
                name: "lufa"
            }
            lufa.path: product.lufa.path
            lufa.mcu: product.lufa.mcu
            lufa.arch: product.lufa.arch
            lufa.board: product.lufa.board
            lufa.f_cpu: product.lufa.f_cpu
            lufa.f_usb: product.lufa.f_usb
            lufa.optimization: product.lufa.optimization
        }
    }

    references: [
        "virtualserial"
    ]
}
