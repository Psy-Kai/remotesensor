import qbs

StaticLibrary {
    name: "Lufa - Virtual Serial"
    targetName: "VirtualSerial"

    files: [
        "Config/LUFAConfig.h",
        "descriptors.c",
        "descriptors.h",
        "virtualserial.c",
        "virtualserial.h",
        "libVirtualSerial.h",
    ]

    Depends {
        name: "LUFA"
    }
    Export {
        cpp.includePaths: product.sourceDirectory

        Depends {
            name: "cpp"
        }
    }

}
