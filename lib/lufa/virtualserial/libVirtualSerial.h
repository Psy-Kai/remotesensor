#pragma once

/*Includes*/
#include <stdio.h>

/*! \file libVirtualSerial.h
 *  \brief LUFA Virtual Serial CDC COM Port - Interface Documentation.
 *
 * \details The LUFA Virtual Serial COM Port allows communication via the USB with the host.
 * This header file serves as interface to the Virtual Serial library.
 *
 */


/* Function Prototypes: */
	/*! Intitializes the virtual serial COM port.
	 */
	void setup_virt_ser_port(void);

	/*!
	 * \brief Sends a message supplied by a pointer via serial COM port.
	 * \fn void send_string(char* message);
	 * \param char* message = "hello world\r\n";
	 *  use sprintf to send numbers
	 */
	void send_string(char* message);

	/*!
	 * \brief Intitializes the virtual serial COM port.
	 * \fn void send_message(char array[]);
	 * \param use plaintext in quotes "hello world\r\n";
	 */
	void send_message(char array[]);
