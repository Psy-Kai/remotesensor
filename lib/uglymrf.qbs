import qbs

StaticLibrary {
    id: root
    name: "ugly mrf"

    property path projectDirPath
    condition: (projectDirPath !== undefined) &&
               (projectDirPath !== "") &&
               (qbs.architecture !== undefined) &&
               (qbs.architecture.startsWith("avr"))

    files: [
        projectDirPath + "/lib/wireless/*.c",
        projectDirPath + "/lib/wireless/*.h",
    ]

    cpp.defines: [
        "F_CPU=16000000UL"
    ]

    Depends {
        name: "cpp"
    }

    Export {
        condition: true // product.condition
        cpp.includePaths: product.projectDirPath + "/lib/wireless"
        Depends {
            name: "cpp"
        }
    }
}
