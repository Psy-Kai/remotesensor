import qbs
import qbs.FileInfo

Module {
    name: "802154"

    property path projectDirPath

    condition: projectDirPath
    cpp.includePaths: [
        FileInfo.joinPaths(projectDirPath, "include")
    ]

    cpp.staticLibraries: [
        FileInfo.toNativeSeparators(FileInfo.joinPaths(projectDirPath,
                                                       "/lib/libCommunicationModule.a"))
    ]

    Depends {
        name: "cpp"
    }
}
